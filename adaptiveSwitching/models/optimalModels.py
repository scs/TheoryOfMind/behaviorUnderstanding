#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: jpoeppel
"""
from __future__ import division

import math
import functools, itertools
from operator import mul
import numpy as np

from .utils import Factor
from . import ToMModel

class ProbModel:

    def __init__(self, variables, outcomes, priors, action_likelihood, observation_model=None, use_softmax=False):
        self.variables = list(variables)
        self.outcomes = dict(outcomes)
        self.prob = Factor(self.variables, self.outcomes, priors)
        self.likelihood = action_likelihood
        self.observation_model = observation_model
        self.use_softmax = use_softmax

    def clone(self):
        return ProbModel(self.variables, self.outcomes, self.prob.potentials, self.likelihood, 
                    self.observation_model, self.use_softmax)

    def update(self, obs, update=True):
        prior = self.prob.copy()
        new_joint = self.likelihood(obs) * self.prob # Factor multiplication (pointwise)
        action_prob = min(np.sum(new_joint.potentials),1) #new_joint.marginalize(self.variables) 

        if update:
            self.prob = new_joint
            self.prob.normalize(soft_max=self.use_softmax)
            if self.observation_model:
                self.prob = self._update_observations(obs, prior)
        return action_prob, self.prob.copy()

    def _update_observations(self, obs, prior):
        prior_marginals = self.marginals(prior)
        marginals = self.marginals()
        posterior = dict(marginals)
        for v in self.observation_model.variables:
            posterior[v] = self.observation_model.likelihood(v, obs) * prior_marginals[v]
            posterior[v].normalize()
        # Update self.prob
        res = functools.reduce(mul, posterior.values())
        return res

    def marginals(self, prob=None, to_dict=False):
        if prob is None:
            prob = self.prob
        res = {}
        for v in self.variables:
            vars = set(self.variables)
            vars.remove(v)
            marg = prob.marginalize(vars)
            if to_dict:
                res[v] = marg.to_dict()
            else:
                res[v] = marg
        return res

class TrueWorldModel(ToMModel):
    """
        This model only considers different "desires", i.e.
        goals the agent may want to reach.
    """

    def __init__(self, env, parameters):
        self.name = "TrueWorldModel"
        self.parameters = dict(parameters)
        self.env = env
        self.variables = ["desire"]
        desire_outcomes = [info["symbol"] for info in parameters["targets"].values()]
        self.true_goal_belief = tuple(desire_outcomes)
        self.true_space_belief = "TrueBelief"
        desire_priors = np.ones(len(desire_outcomes))
        desire_priors /= np.sum(desire_priors)
        self.outcomes = {"desire": desire_outcomes}
        self.prob_model = ProbModel(self.variables, self.outcomes, desire_priors, self.get_action_likelihoods, use_softmax=parameters.get("softmax", False))
        self.last_state = None
        if parameters.get("BiasedGenerativeModel", False):
            self.generative_model = BiasedGenerativeModel(env, parameters)
        else:
            self.generative_model = GenerativeModel(env, parameters)
        self.cur_step = 0

    def clone(self):
        res = TrueWorldModel(self.env, self.parameters)
        return res

    def update(self, observation, update=True, sample=None):
        self.cur_step += 1
        if sample != None:
            self.outcomes = {k:[sample[k]] if k in sample else v for k,v in self.outcomes.items()}
        if self.last_state is None:
            self.last_state = observation
            return 0, self.prob_model.marginals(to_dict=True)
        action_prob, posterior_factor = self.prob_model.update(observation, update)
        self.last_state = observation
        return action_prob, self.prob_model.marginals(posterior_factor, to_dict=True)

    def get_action_likelihoods(self, obs):
        if self.last_state is None:
            return Factor.get_trivial()
        else:
            pass
        outcomes = dict(self.outcomes)
        
        first_state = self.last_state
        second_state = obs
        goal_probs = []
        for goal_desire in outcomes["desire"]:
            prob = self.generative_model.maze_action_likelihood(first_state, second_state, goal_desire,
                            self.true_goal_belief, self.true_space_belief)
            goal_probs.append(prob)
        probs = np.array(goal_probs)
        return Factor(variables=self.variables, outcomes=outcomes, priors=probs)

    

class GoalBeliefObservation:

    def __init__(self, env, affected_variables, outcomes, belief_lookup):
        self.env = env
        self.variables = affected_variables
        self.outcomes = outcomes
        self.belief_lookup = belief_lookup
        self.observe_probability = 1.0 # 0.95
        self.visibility = 15

    def likelihood(self, variable, obs):
        obs_prob = 0
        goal_seen = False
        probs = []
        outcomes = dict(self.outcomes)
        for bg in outcomes[variable]:
            for desire in bg:
                goal_pos = self.belief_lookup[bg][desire]
                # If we see the currently assumed goal position
                if self.env.is_visible(goal_pos, obs, radius=self.visibility):
                    goal_seen = True
                    # the probability of observing the actual goal there is ~1
                    # if bg represents a world where our goal is at that location
                    if self.env.tiles[goal_pos].target_symbol == desire:
                        obs_prob = self.observe_probability
                    # ~0 otherwise
                    else:
                        obs_prob = (1-self.observe_probability)
                        # Do not check the other desires, because this belief has been proven impossible already
                        break
                
                
            if not goal_seen:
                # I did not see any goal -> cannot update my belief
                obs_prob = self.observe_probability
            probs.append(obs_prob)
            
        probs = np.array(probs)
        probs /= sum(probs)
        return Factor(variables=[variable], outcomes=outcomes, priors=probs)

class GoalBeliefModel(ToMModel):
    """
        This model considers different "desires", i.e.
        goals the agent may want to reach and "goal destination beliefs", i.e. where the 
        agent thinks the different goals are located.
    """

    def __init__(self, env, parameters):
        self.name = "GoalBeliefModel"
        self.parameters = dict(parameters)
        self.env = env
        self.variables = ["desire", "goalBelief"]
        desire_outcomes = [info["symbol"] for info in parameters["targets"].values()]
        perms = list(itertools.permutations([info["symbol"] for info in parameters["targets"].values()]))
        
        num_perms = len(perms)
        self.goal_lookup = {info["symbol"]: pos for pos, info in parameters["targets"].items()}
        goal_positions = list(parameters["targets"].keys())
        self.belief_lookup = {perm: {info["symbol"]: goal_positions[perm.index(info["symbol"])] 
                                for info in parameters["targets"].values()} for perm in perms}
        goal_belief_priors = np.ones(shape=(len(desire_outcomes), num_perms))
        goal_belief_priors /= np.sum(goal_belief_priors)
        self.outcomes = {"desire": desire_outcomes, "goalBelief": perms}
        self.prob_model = ProbModel(self.variables, self.outcomes, goal_belief_priors, 
                    self.get_action_likelihoods,
                    observation_model=GoalBeliefObservation(env, ["goalBelief"], {"goalBelief": perms}, self.belief_lookup),
                    use_softmax=parameters.get("softmax", False))
        self.true_space_belief = "TrueBelief"
        self.last_state = None
        if parameters.get("BiasedGenerativeModel", False):
            self.generative_model = BiasedGenerativeModel(env, parameters)
        else:
            self.generative_model = GenerativeModel(env, parameters)
        self.cur_step = 0

    def clone(self):
        res = GoalBeliefModel(self.env, self.parameters)
        return res
        

    def update(self, observation, update=True, sample=None):
        self.cur_step += 1
        if sample != None:
            self.outcomes = {k:[sample[k]] if k in sample else v for k,v in self.outcomes.items()}
        if self.last_state is None:
            self.last_state = observation
            return 0, self.prob_model.marginals(to_dict=True)
        action_prob, posterior_factor = self.prob_model.update(observation, update)
        self.last_state = observation
        return action_prob, self.prob_model.marginals(posterior_factor, to_dict=True)

    def get_action_likelihoods(self, obs):
        if self.last_state is None:
            return Factor.get_trivial()
        else:
            pass
        outcomes = dict(self.outcomes)
        first_state = self.last_state
        second_state = obs
        probs = []
        for goal_desire in outcomes["desire"]:
            belief_probs = []
            for belief in outcomes["goalBelief"]:    
                prob = self.generative_model.maze_action_likelihood(first_state, second_state, goal_desire,
                            belief, self.true_space_belief)
                belief_probs.append(prob)
            probs.append(belief_probs)
            
        probs = np.array(probs)
        return Factor(variables=list(self.variables), outcomes=outcomes, priors=probs)


class FreeSpaceModel(ToMModel):
    """
        This model considers different "desires", i.e.
        goals the agent may want to reach while assuming that there are no obstacles in the unseen 
        environment.
    """

    def __init__(self, env, parameters):
        self.name = "FreeSpaceModel"
        self.parameters = dict(parameters)
        self.env = env
        self.variables = ["desire"]
        desire_outcomes = [info["symbol"] for info in parameters["targets"].values()]
        # desire_priors =  np.ones(shape=(len(self.variables), len(desire_outcomes)))
        self.true_goal_belief = tuple(desire_outcomes)
        self.freespace_belief = "FreeSpace"
        desire_priors = np.ones(len(desire_outcomes))
        desire_priors /= np.sum(desire_priors)
        self.outcomes = {"desire": desire_outcomes}
        self.prob_model = ProbModel(self.variables, self.outcomes, desire_priors, self.get_action_likelihoods, use_softmax=parameters.get("softmax", False))
        self.last_state = None
        if parameters.get("BiasedGenerativeModel", False):
            self.generative_model = BiasedGenerativeModel(env, parameters)
        else:
            self.generative_model = GenerativeModel(env, parameters)
        self.visibles = set([])
        self.cur_step = 0

    def clone(self):
        res = FreeSpaceModel(self.env, self.parameters)
        return res

    def update(self, observation, update=True, sample=None):
        self.cur_step += 1
        if sample != None:
            self.outcomes = {k:[sample[k]] if k in sample else v for k,v in self.outcomes.items()}
        if self.last_state is None:
            self.last_state = observation
            self._update_visibles(observation)
            return 0, self.prob_model.marginals(to_dict=True)

        self._update_visibles(self.last_state)

        action_prob, posterior_factor = self.prob_model.update(observation, update)

        self.last_state = observation
        return action_prob, self.prob_model.marginals(posterior_factor, to_dict=True)
    
    def _update_visibles(self, obs):
        new_visibles = self.env.get_observation(pos=obs, view_radius=3)
        for p in new_visibles:
            self.visibles.add(p)

    def get_action_likelihoods(self, obs):
        if self.last_state is None:
            return Factor.get_trivial()
        else:
            pass
        outcomes = dict(self.outcomes)
        
        first_state = self.last_state
        second_state = obs
        visibles = self.visibles
        goal_probs = []
        for goal_desire in self.outcomes["desire"]:
            prob = self.generative_model.maze_action_likelihood(first_state, second_state, goal_desire,
                            self.true_goal_belief, self.freespace_belief, visibles=visibles)
            goal_probs.append(prob)
        probs = np.array(goal_probs)
        return Factor(variables=self.variables, outcomes=outcomes, priors=probs)

    
class FullBToMModel(ToMModel):

    def __init__(self, env, parameters):
        self.name = "FullBToMModel"
        self.parameters = dict(parameters)
        self.env = env
        self.variables = ["desire", "goalBelief", "spaceBelief"]
        desire_outcomes = [info["symbol"] for info in parameters["targets"].values()]
        perms = list(itertools.permutations([info["symbol"] for info in parameters["targets"].values()]))
        num_perms = len(perms)
        self.goal_lookup = {info["symbol"]: pos for pos, info in parameters["targets"].items()}
        goal_positions = list(parameters["targets"].keys())
        self.belief_lookup = {perm: {info["symbol"]: goal_positions[perm.index(info["symbol"])] 
                                for info in parameters["targets"].values()} for perm in perms}
        belief_priors = np.ones(shape=(len(desire_outcomes), num_perms, 2))
        belief_priors /= np.sum(belief_priors)
        self.outcomes = {"desire": desire_outcomes, "goalBelief": perms, "spaceBelief": ["TrueBelief", "FreeSpace"]}
        self.prob_model = ProbModel(self.variables, self.outcomes, belief_priors, 
                    self.get_action_likelihoods,
                    observation_model=GoalBeliefObservation(env, ["goalBelief"], {"goalBelief": perms}, self.belief_lookup),
                    use_softmax=parameters.get("softmax", False))
        if parameters.get("BiasedGenerativeModel", False):
            self.generative_model = BiasedGenerativeModel(env, parameters)
        else:
            self.generative_model = GenerativeModel(env, parameters)
        self.last_state = None
        self.visibles = set([])
        self.cur_step = 0

    def clone(self):
        res = FullBToMModel(self.env, self.parameters)
        return res

    def update(self, observation, update=True, sample=None):
        self.cur_step += 1
        if sample != None:
            self.outcomes = {k:[sample[k]] if k in sample else v for k,v in self.outcomes.items()}
        if self.last_state is None:
            self.last_state = observation
            self._update_visibles(observation)
            return 0, self.prob_model.marginals(to_dict=True)

        self._update_visibles(self.last_state)

        action_prob, posterior_factor = self.prob_model.update(observation, update)

        self.last_state = observation
        return action_prob, self.prob_model.marginals(posterior_factor, to_dict=True)
    
    def _update_visibles(self, obs):
        new_visibles = self.env.get_observation(pos=obs, view_radius=3)
        for p in new_visibles:
            self.visibles.add(p)


    def get_action_likelihoods(self, obs):
        if self.last_state is None:
            return Factor.get_trivial()
        else:
            pass
        outcomes = dict(self.outcomes)
        first_state = self.last_state
        second_state = obs
        probs = np.zeros((len(outcomes["desire"]), len(outcomes["goalBelief"]), len(outcomes["spaceBelief"])))
        for i, desire in enumerate(outcomes["desire"]):
            for j, goal_belief in enumerate(outcomes["goalBelief"]):    
                for k, space_belief in enumerate(outcomes["spaceBelief"]):
                    prob = self.generative_model.maze_action_likelihood(first_state, second_state, desire,
                            goal_belief, space_belief, visibles=self.visibles)
                    probs[i,j,k] = prob
            
        return Factor(variables=list(self.variables), outcomes=outcomes, priors=probs)


class GenerativeModel:
    """
        This is an implementation of the generative model used for the 2D maze environment
        that can take different space beliefs into account.
    """

    def __init__(self, env, parameters):
        self.env = env 
        self.visibles = set([])
        self.rationality = parameters.get("beta",1.5)
        self.discount = parameters.get("discount", None) # To raise an exception in case I forgot to set it somewhere
        perms = list(itertools.permutations([info["symbol"] for info in parameters["targets"].values()]))
        goal_positions = list(parameters["targets"].keys())
        self.belief_lookup = {perm: {info["symbol"]: goal_positions[perm.index(info["symbol"])] 
                                for info in parameters["targets"].values()} for perm in perms}

    def maze_action_likelihood(self, first_state, second_state, desire, goal_belief, space_belief, visibles=None):
        beta = self.rationality
        discount = self.discount
        goal_pos = self.belief_lookup[goal_belief][desire]
        if space_belief == "TrueBelief":
            norm = 0
            prob = 0
            for n_pos in self.env.tiles[first_state].neighbours:
                n = self.env.tiles[n_pos]
                if n.passable:
                    d = self.env.compute_distance(n.pos, goal_pos)
                    if discount == 1:
                        u = d+1
                    else:
                        u = (1-discount**(d+1))/(1-discount)
                    tmp = math.exp(-beta*u)
                    norm += tmp
                    if n.pos == second_state:
                        prob = tmp
            prob /= norm
            
        elif space_belief == "FreeSpace":
            if visibles is None: 
                visibles = set([])
            self.env.mpaa_initialized = False
            norm = 0
            prob = 0
            for n_pos in self.env.tiles[first_state].neighbours:
                n = self.env.tiles[n_pos]
                if n.passable:
                    d = self.env.compute_distance_partially_visible_mpaa(n_pos, goal_pos, 
                                                                visibles=visibles, observe=False)
                    # d = 1
                    if discount == 1:
                        u = d+1
                    else:
                        u = (1-discount**(d+1))/(1-discount)
                    tmp = math.exp(-beta*u)
                    norm += tmp
                    if n_pos == second_state:
                        prob = tmp
            prob /= norm
        return prob


class BiasedGenerativeModel:
    """
        This is an implementation of the biased generative model used for the 2D maze environment
        that can take different space beliefs into account but adapts the rationality constant beta
        according to the distance to the considered target, favouring closer targets compared to 
        those further away.
    """

    def __init__(self, env, parameters):
        self.env = env 
        self.visibles = set([])
        self.alpha = parameters.get("alpha", 2.5)
        self.gamma = parameters.get("gamma", 0.125)
        perms = list(itertools.permutations([info["symbol"] for info in parameters["targets"].values()]))
        goal_positions = list(parameters["targets"].keys())
        self.belief_lookup = {perm: {info["symbol"]: goal_positions[perm.index(info["symbol"])] 
                                for info in parameters["targets"].values()} for perm in perms}

    def maze_action_likelihood(self, first_state, second_state, desire, goal_belief, space_belief, visibles=None):
        alpha = self.alpha 
        gamma = self.gamma
        goal_pos = self.belief_lookup[goal_belief][desire]
        beta = alpha* math.exp(-gamma*self.env._heuristic(first_state, goal_pos))
        if space_belief == "TrueBelief":
            norm = 0
            prob = 0
            for n_pos in self.env.tiles[first_state].neighbours:
                n = self.env.tiles[n_pos]
                if n.passable:
                    d = self.env.compute_distance(n.pos, goal_pos)
                    tmp = math.exp(-beta*d)
                    norm += tmp
                    if n.pos == second_state:
                        prob = tmp
            prob /= norm
            
        elif space_belief == "FreeSpace":
            if visibles is None: 
                visibles = set([])
            self.env.mpaa_initialized = False
            norm = 0
            prob = 0
            for n_pos in self.env.tiles[first_state].neighbours:
                n = self.env.tiles[n_pos]
                if n.passable:
                    d = self.env.compute_distance_partially_visible_mpaa(n_pos, goal_pos, 
                                                                visibles=visibles, observe=False)
                    tmp = math.exp(-beta*d)
                    norm += tmp
                    if n_pos == second_state:
                        prob = tmp
            prob /= norm
        return prob
