import abc
import numpy as np

class ToMModel(abc.ABC):

    @abc.abstractmethod
    def update(self, observations):
        raise NotImplementedError("Needs to be implemented by subclass")

    def initialize(self, episode, probs, full_update=False):
        if full_update:
            if self.cur_step < len(episode):
                for state in episode[self.cur_step:]:
                    action_prob, distri = self.update(state)
        else:
            self.last_state = episode[-1] if len(episode) > 0 else None


    def get_action_predictions(self, observations):
        # Should return the P(a) = \sum_m P(a|m)*\prod_mi P(mi) for all possible actions (which these classes 
        # currently do not know about), required to compute surprise measure
        res = {}
        for obs in observations:
            action_prob, _ = self.prob_model.update(obs, update=False)
            res[obs] = action_prob
        return res

    def bias(self, variable, outcome, val=1):
        
        prob_clone = self.prob_model.clone()
        variable_idx = prob_clone.variables.index(variable)
        outcome_idx = prob_clone.outcomes[variable].index(outcome)
        potentials = prob_clone.prob.potentials.copy()
        mask = np.ones(len(potentials), dtype=bool)
        mask[outcome_idx] = 0
        if len(prob_clone.outcomes) > 1:
            potentials[mask,:] = 0
            potentials[outcome_idx,:] /= np.sum(potentials)
        else:
            potentials[mask] = 0
            potentials[outcome_idx] /= sum(potentials)
        prob_clone.prob.potentials = potentials

        self.prob_model = prob_clone.clone()