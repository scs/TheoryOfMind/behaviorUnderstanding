import re

def extract_condition_identifiers(condition):
    matchObj = re.match("condMap(\d{1,})_C(\d{1,})_V(\d{1,})", condition)
    if matchObj:
        map_n = matchObj.group(1)
        c_n = matchObj.group(2)
        v_n = matchObj.group(3)

        return map_n, c_n, v_n