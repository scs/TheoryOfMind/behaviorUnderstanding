import queue, os, logging, logging.handlers, sys, pathlib, re, math
base_path = os.path.abspath(os.path.dirname(__file__) + "/..")
sys.path.append(base_path)

import adaptiveSwitching.playback as playback
import adaptiveSwitching.observer as observer
from adaptiveSwitching.utils import extract_condition_identifiers

def rate_episode_flexible(env, targets, agent, true_goal, 
                    strategy="switching", condition=None, 
                    strategy_parameters=None, model_parameters=None):

    mentalizer = observer.Mentalizer(selection_strategy=strategy, selector_parameters=strategy_parameters,
                    model_parameters=model_parameters) # Initialize new Observer for each episode?
    mentalizer.initialize_tom_models(env, targets)#, use_softmax)
    episode = agent.unroll()
    results = {"probs": [], "models": [], "predicted_score": [], 
                "accumulated_scores": [], "inferred_state": [], 
                "metrics": [], "miscs": [], "regression_params":None}


    extra_data = None
    # if use_features and condition != None:
    if strategy_parameters["adaptive"].get("use_features",False) and condition != None:
        match_obj = re.match("condMap(\d{1,})_C(\d{1,})_V(\d{1,})", condition)
        if match_obj:
            map_n = match_obj.group(1)
            c_n = match_obj.group(2)
            v_n = match_obj.group(3) 
            extra_data = {"map_id": int(map_n), "condition": int(c_n), "variant": int(v_n)}

    for state in episode:
        # print("currently at state: ", state)
        prob, used_model, predicted_score, accumulated_score, others_model, metrics = mentalizer.update(state, extra=extra_data)
        visibles = mentalizer.model_selector.visibles 
        results["probs"].append(prob)
        results["models"].append(used_model)
        results["predicted_score"].append(predicted_score)
        results["accumulated_scores"].append(accumulated_score)
        results["inferred_state"].append(others_model)
        results["metrics"].append(metrics)
        results["miscs"].append(list(visibles))
    
    regression_params = None
    if strategy == "adaptive":
        regression_params = mentalizer.model_selector.save_regression_model()
        results["regression_params"] = regression_params

    return results, regression_params

def evaluate_model(participant_data, strategy_parameters, model_parameters, strategy="switching"):
    conditions = participant_data
    all_results = {}
    for i, condition in enumerate(conditions):
        if not condition in all_results:
            all_results[condition] = []
        print("Working on condition {}(#{})".format(condition, i))
        for env, targets, agent, true_goal in conditions[condition]:
            results = rate_episode_flexible(env, targets, agent, true_goal, strategy=strategy, 
                                            strategy_parameters=strategy_parameters, model_parameters=model_parameters)

            print("results: ", results)
            all_results[condition].append(results)

    save_results(all_results)

def save_results(results):
    PATH = "./"
    for condition in results:
        with open(PATH+"_"+condition+".csv", "w") as f:
            f.write("Run;Step;Prob;SelectedModel;PredictedScore;OtherState;Time\n")
            for run in range(len(results[condition])):
                data = results[condition][run]
                for step in range(len(data["probs"])):
                    f.write("{};{};{};{};{};{}\n".format(run, step, data["probs"][step], 
                                    data["models"][step].name, 
                                    data["predicted_score"][step],
                                    data["inferred_state"][step],
                                    data["metrics"][step]))
    

def extract_timings(actions, only_differnet=False):
    last = actions[0]
    durations = []
    for a in actions[1:]:
        if only_differnet and last[1] == a[1]:
            last = a
            continue
        duration = a[0] - last[0]
        durations.append(duration.total_seconds())
        last = a
    return durations

def evaluate_participant_timings(participant_data, only_different=False):
    timings = {}
    for i, condition in enumerate(participant_data):
        map_n, c_n, v_n = extract_condition_identifiers(condition)
        if not c_n in timings:
            timings[c_n] = {}
        if not map_n in timings[c_n]:
            timings[c_n][map_n] = {}
        if not v_n in timings[c_n][map_n]:
            timings[c_n][map_n][v_n] = []
        for env, targets, agent, true_goal in participant_data[condition]:
            timings[c_n][map_n][v_n].append(extract_timings(agent.actions, only_differnet=only_different))

    return timings
            
            


if __name__ == "__main__":

    strategy_parameters = {'use_softmax': False, 
                        'full_initialization': False, 
                        'adaptive': {'keep_mentalizer': False, 
                                    'use_features': False, 
                                    'opportunity_costs': 2.5, 
                                    'time_prior': {'TrueWorldModel': 0, 'GoalBeliefModel': 0, 'FreeSpaceModel': 0}, 
                                    'score_f': 'NLL', # prob/NLL/S5
                                    'num_features': 3,
                                    'load_regression': None,
                                    # Bayesian Regression Init
                                    'alpha_prior_reward': 10, 
                                    'beta_prior_reward': 1,
                                    'alpha_prior_time': 10,
                                    'beta_prior_time': 1,
                                    "reward_weight": 10, # Scaling factor for reward scores
                                    "time_weight": 1000, # Scaling factor for time scores
                                    },
                        "switching": { "initial_threshold": 20, #1.2 for S5, #20 for NLL 
                                        "threshold_growth": 1.5,
                                        "score_f": "NLL"
                                    }
                        }
    model_parameters = {'BiasedGenerativeModel': False, 'beta':1, 'discount':1, 'alpha': 2.5, 'gamma': 0.125}

    ## Setting up conditions path for playback:
    base_path = "data/aamas"
    playback.CONDITION_PATH = base_path + os.path.sep + "Conditions"
    ## Setting up loggers
    que = queue.Queue(-1)  # no limit on size
    queue_handler = logging.handlers.QueueHandler(que)
    handler = logging.FileHandler("logFile.txt")
    listener = logging.handlers.QueueListener(que, handler)
    root = logging.getLogger()
    root.addHandler(queue_handler)
    listener.start()

    ## Path towards the result files
    path = base_path + os.path.sep + "Participant_data/"
    participant_data = playback.crawl_results(path)
    evaluate_model(participant_data, strategy_parameters, model_parameters, "adaptive")    
    timings = evaluate_participant_timings(participant_data)
    
    listener.stop()
