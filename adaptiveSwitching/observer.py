
import time
import numpy as np
import math
from math import log10 as mlog10
from scipy import stats

import adaptiveSwitching.models.optimalModels as models


def get_observations(state):
    obs = []
    x,y = state 
    for i,j in [(1,0),(-1,0),(0,1),(0,-1)]:
        obs.append((x+i,y+j))
    return obs

# l10le = 4.342944819

class BayesLinReg:
    """
        Bayesian Linear Regression with unknown mean and unknown variance/precision
        which therefore uses a normal-gamma prior distribution for the two parameters
        $p(w, \beta)$ (see Bishop, Exercise 3.12)
    """
    def __init__(self, name, n_features, mean_0, cov_0, alpha_0, beta_0):
        self.name = name
        self.n_features = n_features
        self.alpha = alpha_0
        self.beta = beta_0
        self.mean = np.ones(n_features)*mean_0
        self.cov_inv = np.identity(n_features)/cov_0
        self.cov = np.identity(n_features)*cov_0

    def learn(self, x, y):
        # Updating inverse covariance for normal-gamma
        # Taken from solution to Exercise 3.12: 
        # https://github.com/zhengqigao/PRML-Solution-Manual/blob/master/PRML_Solution_Manual.pdf
        inv_cov_1 = self.cov_inv + np.outer(x, x)
        cov_1 =  np.linalg.inv(inv_cov_1)
        mean_1 = cov_1 @ (self.cov_inv @ self.mean + y*x)
        beta_1 = self.beta + 0.5*(y*y + self.mean.T @ self.cov_inv @ self.mean - mean_1.T @ inv_cov_1 @ mean_1)
        alpha_1 = self.alpha + 0.5
        self.cov_inv = inv_cov_1
        self.cov = cov_1
        self.alpha = alpha_1
        self.beta = beta_1
        self.mean = mean_1

    def sample_weights(self):
        cov = self.cov
        gamma = self.alpha / self.beta
        weights_rv = stats.multivariate_normal(mean=self.mean, cov=cov/gamma)
        return weights_rv.rvs()

    def to_params(self):
        return {"name": self.name, "n_features": self.n_features, "mean":self.mean, 
                "alpha": self.alpha, "beta":self.beta, "cov_inv": self.cov_inv}

    @classmethod
    def from_parameters(cls, pars):
        res = cls(pars["name"], pars["n_features"], mean_0=pars["mean"],cov_0=1, 
                                                alpha_0=pars["alpha"], beta_0=pars["beta"])
        res.cov_inv = pars["cov_inv"]
        return res

class AdaptiveSelector:
    """
        The adaptive switching selection stragey from the thesis. This model trains and updates
        linear regression models for the predict reward and time requirements of each model 
        and selects the model to be used based on these predictions. Only the model that was used 
        for a prediction is updated based on the environments feedback.
    """

    def __init__(self, model_store: dict, parameters: dict): 
        self.model_store = model_store
        self.current_model = None
        self.parameters = dict(parameters)

        reward_priors = dict(parameters.get("reward_prior", {m: 0 for m in model_store}))
        time_priors = dict(parameters.get("time_prior", {m: 0 for m in model_store}))
        opportunity_costs_prior = parameters.get("opportunity_costs", 1)
        n_features = parameters.get("num_features", 3)

        self.reward_priors = reward_priors
        self.time_priors = time_priors
        self.n_features = n_features
        self.opportunity_costs_prior = opportunity_costs_prior
        self.cur_score = 0
        self._init()

    def _init(self):
        if self.parameters.get("load_regression", None):
            regression_params = self.parameters["load_regression"]
            self.reward_regressions = {m: BayesLinReg.from_parameters(regression_params["reward_r"][m]) for m in self.model_store}
            self.time_regressions = {m: BayesLinReg.from_parameters(regression_params["time_r"][m]) for m in self.model_store}
        else:
            self.reward_regressions = {m: BayesLinReg("Reward Regression", self.n_features, self.reward_priors[m], 1, self.parameters.get("alpha_prior_reward",1), self.parameters.get("beta_prior_reward",1)) for m in self.model_store}
            self.time_regressions = {m: BayesLinReg("Time Regression", self.n_features, self.time_priors[m], 1,  self.parameters.get("alpha_prior_time",1), self.parameters.get("beta_prior_time",1)) for m in self.model_store}
        self.p_max = 1 #Init
        self.opportunity_costs = self.opportunity_costs_prior # for now, consider changing this to a regression later as well!
        self.choose_model(None)

    def choose_model(self, observation, extra=None):
        best_score = -np.inf
        best_model = None 
        features = self._extract_features(observation, extra)

        for m in self.model_store:
            predicted_performance = self._predict_performance_linear(m, features)
            if predicted_performance > best_score:
                best_model = m 
                best_score = predicted_performance
        self.current_model = best_model

        return self.model_store[best_model], best_score


    def _extract_features(self, observation, extra=None) -> np.array:
        if extra is None:
            f = np.array([1]*self.n_features)
        else:
            if self.n_features == 5:
                if observation is None:
                    return np.array([1]*self.n_features) # Default features 
                f = [*observation, extra["map_id"], extra["variant"], extra["condition"]]
            elif self.n_features == 2:
                f = [1, extra["condition"]]
            elif self.n_features == 3:
                f = [0, 0, 0]
                f[extra["condition"]-1] = 1
        return np.array(f)

    def update_estimates(self, observations, reward, time_taken, total_rewards, total_time, extra=None):
        # last_model = self.current_model
        features = self._extract_features(observations, extra)
        reward_weight = self.parameters.get("reward_weight", 1)
        time_weight = self.parameters.get("time_weight", 1)
        if self.parameters["score_f"] == "prob":
            reward_score = reward*reward_weight
            self.reward_regressions[self.current_model].learn(features, reward_score)
        elif self.parameters["score_f"] == "NLL":
            reward_score = mlog10(reward)*reward_weight if reward > 0 else 0
            self.reward_regressions[self.current_model].learn(features, reward_score)
        elif self.parameters["score_f"] == "S5":
            p_max = self.p_max
            reward_score = mlog10(1+p_max-reward)*reward_weight
            self.reward_regressions[self.current_model].learn(features, reward_score)
        # Blow up time to be more comparable to the reward values
        self.time_regressions[self.current_model].learn(features, time_taken*time_weight) 

    def update_score(self, prob):
        if prob > 0:
            self.cur_score += -math.log(prob)

    def _predict_performance_linear(self, model, features):
        """
            Lieder and Griffiths propose 

            $$ V_t(s, problem) = \sum_{k=1}^n w_{k,s}^{(R)} \cdot f_k(problem) - \mathop{\mathbb{E}}[\gamma|h_t] \cdot \sum_{k=1}^n w_{k,s}^{(T)} \cdot f_k(problem) $$
            for strategy/model s, where these weights are sampled from distributions
            learned as Bayesian linear regression.
        """
        reward_weights = self.reward_regressions[model].sample_weights()
        time_weights = self.time_regressions[model].sample_weights()
        performance = reward_weights.T @ features - self.opportunity_costs * time_weights.T @ features
        return performance

    @property
    def visibles(self):
        if hasattr(self.model_store, "FreeSpaceModel"):
            if hasattr(self.model_store["FreeSpaceModel"], "visibles"):
                return list(self.model_store["FreeSpaceModel"].visibles)
        return []

    def save_regression_model(self):
        return {"reward_r": {m:br.to_params() for m,br in self.reward_regressions.items()},
                "time_r":  {m:br.to_params() for m,br in self.time_regressions.items()} }

class SwitchingSelector:
    """
        The switching baseline selection model from the thesis which always sticks with the 
        current model for as long as a given, dynamic threshold is not passed. If the threshold
        is passed, it selects evaluates all its known models to find the one performing currently
        the best.
    """

    def __init__(self, model_store, parameters, initial_model):
        self.model_store = model_store
        self.parameters = dict(parameters)
        self.initial_threshold = parameters.get("initial_threshold", 20)
        self.threshold = self.initial_threshold
        self.threshold_growth = parameters.get("threshold_growth", 1.5)
        self.episode = []
        self.cur_score = 0
        self.acc_surprise = 0
        self.current_model = initial_model

        

    def update_estimates(self, observations, reward, time_taken, total_rewards, total_time, extra=None):
        # This model does not update any estimates but we want to implement the same interface
        # for the observer
        pass 

    def update_score(self, prob):
        if prob > 0:
            self.cur_score += -math.log(prob)
        
        if self.parameters.get("score_f", "NLL") == "NLL":
            self.acc_surprise += -math.log(prob) if prob > 0 else 0
        elif self.parameters.get("score_f", "NLL") == "S5":
            self.acc_surprise += math.log(1+self.p_max-prob)

    def choose_model(self, observation, extra=None):
        best_model = self.current_model

        if self.acc_surprise >= self.threshold:
            scores = {m: 0 for m in self.model_store}
            others = {}
            for m in self.model_store:
                if m == self.current_model:
                    scores[m] = self.acc_surprise
                    continue
                scores[m], self.model_store[m], others[m] = self._evaluate_model(self.model_store[m])
            
            best_model, best_score = sorted(scores.items(), key= lambda x: x[1])[0]
            self.threshold *= self.threshold_growth
            self.current_model = best_model
            self.acc_surprise = best_score
        self.episode.append(observation)
        
        return self.model_store[best_model], self.cur_score#, other

    def _evaluate_model(self, m):
        clean_model = m.clone()
        # Manually initialize here since we compute the score here manually 
        if len(self.episode) < 2:
            return 0, clean_model, None

        clean_model.cur_step = 1    
        clean_model.last_state = self.episode[0]
        score = 0
        for obs in self.episode[1:]:
            if self.parameters.get("score_f", "NLL") == "NLL":
                action_prob, other = clean_model.update(obs)
                surprise = -math.log(action_prob)
            elif self.parameters.get("score_f", "NLL") == "S5":
                observations = get_observations(clean_model.last_state)
                action_probs = clean_model.get_action_predictions(observations)
                p_max = max(action_probs.values())
                action_prob, other = clean_model.update(obs)
                surprise = math.log(1+p_max-action_prob) 
            score += surprise

        return score, clean_model, other

    @property
    def visibles(self):
        if hasattr(self.model_store[self.current_model], "visibles"):
            return list(self.model_store[self.current_model].visibles)
        return []


class StaticModel:
    """
        Trivial model that does not do any switching but just sticks with the
        given model. This implements the same interface as the dynamic models 
        to allow for better comparison with identical code for the observer.
    """

    def __init__(self, btom_model):
        self.model_store = {"model": btom_model}
        self.current_model = btom_model
        self.cur_score = 0
        self.parameters = {}

    def update_estimates(self, observations, reward, time_taken, total_rewards, total_time, extra=None):
        # This model does not update any estimates but we want to implement the same interface
        # for the observer
        pass

    def update_score(self, prob):
        if prob > 0:
            self.cur_score += -math.log(prob)

    def choose_model(self, observation, extra=None):
        return self.current_model, self.cur_score

    @property
    def visibles(self):
        if hasattr(self.current_model, "visibles"):
            return list(self.current_model.visibles)
        return []


class Measurement:

    def __init__(self):
        self._reset()

    def _reset(self):
        self.start_t = time.perf_counter()
        self.t0 = None
        self.dt = 0 
        self.timings = []

    @property
    def ellapsed_time(self):
        return time.perf_counter() - self.start_t

    def __enter__(self):
        self.t0 = time.perf_counter()

    def __exit__(self, *args):
        # print("measurement exit: ", args)
        self.dt = (time.perf_counter() - self.t0) # Store the desired value.
        self.timings.append(self.dt)

class Mentalizer:

    def __init__(self, selection_strategy="switching", selector_parameters=None, model_parameters=None):
        if selector_parameters is None:
            selector_parameters = {}
        if model_parameters is None:
            model_parameters = {}

        self.initialize_fully = selector_parameters.get("full_initialization", False)
        self.other_model: dict = None # A presentation of the other agent as a collection of mental states and their distributions
        self.model_store = None
        self.selection_strategy = selection_strategy
        self.model_selector = None # Needs to be initialized when tom models are created
        self.selector_parameters = selector_parameters
        self.model_parameters = model_parameters
        self.regression_models = None
        self.last_state = None
        self.last_prediction = None
        self.last_action_prob = None
        self.reward_total = 0 # Reward is a strong word here, but we'll try this for now
        self.measurements = Measurement()
        self.selection_measurements = Measurement()
        # self.score_list = []
        self.episode = []

    def determine_reward(self, observation):
        """
            For now, make this really simple in that we just consider the action probability
            the last model produced as the "reward": Better explainability of the action 
            should be rewarded. 
        """
        if self.last_action_prob != None:
            self.reward_total += self.last_action_prob
            return self.last_action_prob, self.reward_total
        return 0, 0

    def update(self, observations, extra=None):
        """
            Updates the current model selector, the currently used 
            model.

            Returns the likelihood of the observation (or rather the transition from the 
            last state to this one if a previous existed) as well as the model used
            to infer that likelihood and other metrics.
        """
        
        # print("ment update obs: ", observations)
        reward, total_rewards = self.determine_reward(observations)
        time_taken = self.measurements.dt 
        total_time = self.measurements.ellapsed_time
        with self.selection_measurements:
            self.model_selector.update_estimates(self.last_state, reward, time_taken, total_rewards, total_time, extra=extra)  # Update model's predicted rewards and timings based on the last observations
            tom_model, predicted_score = self.model_selector.choose_model(observations, extra=extra)
            # print("selected model: {} (step: {}) len ep: {}".format(tom_model.name, tom_model.cur_step, len(self.episode)))
        # tom_model = self.model_selector.current_model
            tom_model.initialize(self.episode, self.other_model, full_update=self.initialize_fully) # Try initialization with entire episode to avoid schizophreniac models
        
        # tom_model.initialize(self.last_state, self.other_model) #This may not work properly, maybe we need to initialize from last x observations
        with self.measurements:
            action_prob, self.other_model = tom_model.update(observations)
            # print("Action prob for obs {}: {}".format(observations, action_prob))
            # print("ToM model update: ", time.perf_counter()-t0)

            if self.model_selector.parameters.get("score_f", "NLL") == "S5":
                observations_ = get_observations(observations)
                action_probs = tom_model.get_action_predictions(observations_)
                # print("action probs: ", action_probs)
                self.model_selector.p_max = max(action_probs.values())

        self.model_selector.update_score(action_prob)

        self.episode.append(observations)

        self.last_action_prob = action_prob
        self.last_state = observations
        accumulated_score = self.model_selector.cur_score
        return action_prob, tom_model, predicted_score, accumulated_score, self.other_model, {"time_taken": self.measurements.dt, "selection_time": self.selection_measurements.dt}


    def initialize_tom_models(self, env, targets, 
                        models_to_init=("TrueWorldModel", "GoalBeliefModel", "FreeSpaceModel")):
        """
            Populates the model store with models initialized for
            the current task.
        """
        model_parameter = {}
        if not self.model_parameters is None:
            model_parameter = dict(self.model_parameters)

        model_params = {"targets": targets, 
                        "softmax": self.selector_parameters["use_softmax"], 
                        **model_parameter}
        
        model_store = {}
        for m in models_to_init:
            model_store[m] = getattr(models, m)(env, model_params)
        # model_store = {
        #                 "TrueWorldModel": models.TrueWorldModel(env, model_params),
        #                 "GoalBeliefModel": models.GoalBeliefModel(env, model_params),
        #                 "FreeSpaceModel": models.FreeSpaceModel(env, model_params),
        #                }
        if self.selection_strategy == "adaptive":
            self.model_selector = AdaptiveSelector(model_store, parameters=self.selector_parameters["adaptive"])
        elif self.selection_strategy == "switching":
            self.model_selector = SwitchingSelector(model_store, 
                                                parameters=self.selector_parameters["switching"],
                                                    initial_model=list(model_store.keys())[0])
        elif self.selection_strategy in ("BToM", "None"):
            self.model_selector = StaticModel(models.FullBToMModel(env, model_params))
        elif self.selection_strategy == "TrueWorld":
            self.model_selector = StaticModel(models.TrueWorldModel(env, model_params))
        elif self.selection_strategy == "GoalBelief":
            self.model_selector = StaticModel(models.GoalBeliefModel(env, model_params))
        elif self.selection_strategy == "FreeSpace":
            self.model_selector = StaticModel(models.FreeSpaceModel(env, model_params))
        else:
            raise ValueError("Unknown selection strategy set for mentalizier: {}".format(self.selection_strategy))

    def bias(self, variable, outcome, value=1):
        for m in self.model_selector.model_store.values():
            m.bias(variable, outcome, value)
