from posixpath import basename
import json, itertools, os, time, random, re, pickle, ast
from dateutil.parser import parse
import multiprocessing, functools
from pathlib import Path
from ast import literal_eval
import numpy as np
import scipy

from adaptiveSwitching import playback

import adaptiveSwitching.main as main
from adaptiveSwitching.observer import Mentalizer, get_observations
from adaptiveSwitching.blockworld import GridEnvironment 

USE_CACHING = True


class NumpyArrayEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NumpyArrayEncoder, self).default(obj)

NAME_MAP = {"switching": "\switching",
            "TrueWorld": "\gls{TWD}",
            "GoalBelief": "\gls{TW}",
            "FreeSpace": "\gls{TD}",
            "BToM": "\gls{BToM}",
            "adaptive": "\adaptive"
             }

def convert_numpy(o):
    if type(o).__module__ == np.__name__: 
        return o.item()
    return o

def compute_fair_scores(probs):
    return  [sum([-np.log(prob) if prob > 0 else 0 for prob in probs[:t+1]]) for t in range(len(probs))]

class ExperimentResult:

    def __init__(self, strategy, **params):
        self.strategy = strategy
        self.params = dict(params)
        self.run_results = {}
        self.regression_params = {}
        self.final_scores = {}
        self.last_regression_params = None

    def add_run(self, run_result, condition):
        if not condition in self.run_results:
            self.run_results[condition] = []
            self.final_scores[condition] = []

        fair_scores = compute_fair_scores(run_result["probs"])
        run_result["fair_scores"] = fair_scores
        self.run_results[condition].append(run_result)
        self.final_scores[condition].append(run_result["accumulated_scores"][-1])
        self.last_regression_params = run_result["regression_params"]

    @property
    def overall_score(self):
        return np.mean([s for sc in self.final_scores.values() for s in sc])


    @property
    def fair_scores(self):
        scores = {c: [] for c in self.run_results}
        for c in self.run_results:
            for run in self.run_results[c]:
                scores[c].append(run["fair_scores"])
        return scores

    def write_scores_timings(self, path):
        Path(path).mkdir(parents=True, exist_ok=True)
        for condition in self.run_results:
            with open(path+os.path.sep + condition+".csv", "w") as f:
                f.write("Run_Nr;Scorelist;FairScoreList;TimingsAction;TimingsSelection;TimingsCombined\n")
                for i, run_res in enumerate(self.run_results[condition]):
                    score_list = list(run_res["accumulated_scores"]) 
                    fair_score_list = list(run_res["fair_scores"]) #self.fair_scores[condition]
                    timings_action = [metric["time_taken"] for metric in run_res["metrics"]]
                    timings_selection = [metric["selection_time"] for metric in run_res["metrics"]]
                    timings_combined = [metric["time_taken"] + metric["selection_time"] for metric in run_res["metrics"]]
                    f.write("{};{};{};{};{};{}\n".format(i, score_list, fair_score_list, timings_action, 
                                                                    timings_selection, timings_combined))


    def write_regression_params(self, path):
        Path(path).mkdir(parents=True, exist_ok=True)
        if self.last_regression_params != None:
            with open(path+os.path.sep + "collectiveRegressionParams.csv", "w") as f:
                f.write(json.dumps(self.last_regression_params, cls=NumpyArrayEncoder)) #default=convert_numpy))
                # f.write("Reward: {}".format(self.last_regression_params["reward_r"]))
                # f.write("Time: {}".format(self.last_regression_params["time_r"]))

    def write_model_params(self, path, file_name="modelParams.csv"):
        Path(path).mkdir(parents=True, exist_ok=True)
        params = {"strategy_parameter": dict(self.params.get("strategy_parameter", {})),
                    "model_parameter": dict(self.params.get("model_parameter", {}))}
        with open(path+os.path.sep + file_name, "w") as f:
            f.write(json.dumps(params, cls=NumpyArrayEncoder))

    def to_dict(self):
        run_res = dict(self.run_results)
        for con in run_res:
            for i, run in enumerate(run_res[con]):
                run_res[con][i]["models"] = [m if isinstance(m, str) else m.name for m in run_res[con][i]["models"]]
                for s in run["inferred_state"]:
                    if "goalBelief" in s:
                        s["goalBelief"] = {",".join(k):v for k,v in s["goalBelief"].items() }

        return {"strategy": self.strategy,
                "parameter": self.params,
                "overall_score": self.overall_score,
                "all_results": run_res,
                "all_final_scores": self.final_scores,
                "final_regression_params": self.last_regression_params}

    @classmethod
    def from_json(cls, path):
        with open(path, "r") as f:
            _json = json.load(f)
        
        res = cls(_json["strategy"], **_json["parameter"])
        for con in _json["all_results"]:
            for run in _json["all_results"][con]:
                res.add_run(run, con)
        return res

    def dump(self, path, file_name="results.json", pickle=False):
        if pickle:
            f_name = file_name.split(".")[0]
            with open(path+os.path.sep+"{}.pickle".format(f_name), "w") as f:
                pickle.dump(self, f)
        else:
            with open(path+os.path.sep+file_name, "w") as f:
                    f.write(json.dumps(self.to_dict(), cls=NumpyArrayEncoder))

def create_heatmap_data(path, result_path, map_name="condMap6", variant="V1"):
    conditions = playback.crawl_results(path + os.path.sep + "Participant_data")
    visits = {}
    cons = (1,2,3)
    for condition in cons:
        con = "{}_C{}_{}".format(map_name,condition, variant)
        traj = []
        for (env, targets, agent, true_goal) in conditions[con]:
            traj.append(agent.get_positions(ignore_duds=True))

        matrix = np.zeros(env.size)

        for t in traj:
            for pos in t:
                matrix[pos[0]][pos[1]] += 1

        start_pos = agent.start_pos
        matrix[start_pos] = 0 
        matrix[true_goal] = 0
        visits[condition] = matrix

    with open(result_path + os.path.sep + "visits{}{}.csv".format(map_name,variant), "w") as f:
        header="X;Y"
        for con in cons:
            header += ";CountsC{}".format(con)
        f.write(header + "\n")
        for iy, ix in np.ndindex(matrix.shape):
            row = "{};{}".format(ix, iy)
            for con in cons:
                row += ";{}".format(visits[con][iy,ix] if visits[con][iy,ix] != 0 else "nan")
            f.write(row + "\n")
        print("finished writing")

    

def rate_episodes(path, results_path, strategy="switching", strategy_parameters=None, 
                                                    model_parameters=None, conditions=None):
    if conditions is None:
        conditions = playback.crawl_results(path, use_caching=USE_CACHING)

    if strategy_parameters is None:
        strategy_parameters = {}

    running_params = dict(strategy_parameters)
    t0 = time.perf_counter()
    results = ExperimentResult(strategy, strategy_parameter=strategy_parameters, model_parameter=model_parameters)

    for i, condition in enumerate(conditions):
        print("working on condition: ", condition)
        for env, targets, agent, true_goal in conditions[condition]:
            agent.reset(force=True)
            run_res, new_regression_params = main.rate_episode_flexible(env, targets, agent, true_goal,
                                strategy=strategy, condition=condition, strategy_parameters=running_params, model_parameters=model_parameters)
            results.add_run(run_res, condition)
            if strategy in strategy_parameters and  strategy_parameters[strategy].get("keep_mentalizer", False):
                running_params["adaptive"]["load_regression"] = new_regression_params


    results.write_scores_timings(results_path)
    results.write_regression_params(results_path)
    results.write_model_params(results_path)
    results.dump(results_path)
    print("Strategy {} (Softmax {}) took: {}".format(strategy, strategy_parameters.get("use_softmax", False), time.perf_counter()-t0))
    return results 


def read_experiments(base_path):
    results = {}
    methods_results = os.listdir(base_path)
    methods_results = [m for m in methods_results if not ".csv" in m and not "plots" in m and not ".json" in m]
    for method in methods_results:
        if not method in results:
            results[method] = {}
            method_path = base_path + os.path.sep + method
            if os.path.exists(os.path.join(method_path,"results.pickle")):
                pickle_path = os.path.join(method_path,"results.pickle")
                with open(pickle_path, "rb") as f:
                    results[method] = pickle.load(f)
            else:
                json_path =  os.path.join(method_path,"results.json")
                results[method] = ExperimentResult.from_json(json_path)
        
    return results

def read_results(base_path):
    results = {}
    methods_results = os.listdir(base_path)
    methods_results = [m for m in methods_results if not ".csv" in m and not "plots" in m and not ".json" in m]
    for method in methods_results:
        if not method in results:
            results[method] = {}
        path = base_path + os.path.sep + method
        result_files = os.listdir(path)
        result_files = [rf for rf in result_files if "csv" in rf and not "RegressionParams" in rf and not "modelParams" in rf]
        for r_file in result_files:
            r_path = base_path + os.path.sep + method + os.path.sep + r_file
            map_id, con, variant = r_file.split("_")
            variant = variant.split(".")[0]
            if not con in results[method]:
                results[method][con] = {}
            if not map_id in results[method][con]:
                results[method][con][map_id] = []
            with open(r_path, "r") as f:
                for i, line in enumerate(f):
                    if i == 0:
                        continue 
                    splits = line.strip().split(";")
                    if len(splits) == 6:
                        run_nr, score_list, fair_score_list, timings_action, timings_selection, timings_combined = splits
                    else:
                        run_nr, score_list, timings_action, timings_selection, timings_combined = splits
                        fair_score_list = "[0]"

                    score_list = literal_eval(score_list)
                    fair_score_list = literal_eval(fair_score_list)
                    timings_action = literal_eval(timings_action)
                    timings_selection = literal_eval(timings_selection)
                    timings_combined = literal_eval(timings_combined)

                    results[method][con][map_id].append({"Run":run_nr, "scores": score_list, 
                                                        "fair_scores": fair_score_list,
                                                        "timings_action": timings_action,
                                                        "timings_selection": timings_selection,
                                                        "timings_combined": timings_combined})

    return results

def compute_winning_percentages(base_path, file_path=None, all_results=None):
    """
        Computes for each condition and each combination of models in what
        percentage of runs, a model outperforms the other model. As all models
        produce deterministic results, this comparison provides arguably the 
        best measurement for how good the models are.

        Parameters
        ----------
        results: dict
            A dictionary containing the results for the different models to be
            considered (Structure as returned by "rate_entire_episodes")
    """

    # Re-organize results
    conditions = {"C1": {}, "C2": {}, "C3": {}, "Overall": {}}

    if all_results is None:
        all_results = read_results(base_path)


    for model in all_results:
        if not model in conditions["C1"]:
            conditions["C1"][model] = [] 
            conditions["C2"][model] = [] 
            conditions["C3"][model] = [] 

        m_results = all_results[model]
        for con in m_results:
            for map_id in m_results[con]:
                for run in m_results[con][map_id]:
                    conditions[con][model].append(run["fair_scores"][-1])
          
    # Collect "overall"
    for con in conditions:
        if con != "Overall":
            for model in conditions[con]:
                if model in conditions["Overall"]:
                    conditions["Overall"][model].extend(conditions[con][model])
                else:
                    conditions["Overall"][model] = list(conditions[con][model])


    winnings = {}
    for con in conditions:
        winnings[con] = {}
        print("Results for condition: {}".format(con))
        for m1 in conditions[con]:
            winnings[con][m1] = {}
            for m2 in conditions[con]:
                if m1 != m2:
                    counter_better = 0
                    counter_worse = 0
                    total = len(conditions[con][m1])
                    worse = []
                    worse_percent = []
                    for run_nr, score1 in enumerate(conditions[con][m1]):
                        score2 = conditions[con][m2][run_nr]
                        if score1 <= score2:
                            counter_better += 1
                        else:
                            counter_worse += 1
                            worse.append(score1-score2)
                            worse_percent.append((score1-score2)/score2 * 100)
                    winnings[con][m1][m2] = (counter_better, counter_worse, total, np.mean(worse), np.mean(worse_percent))
                else:
                    winnings[con][m1][m2] = (None,None,None,None,None)

    for con in winnings:
        print("Winnings percentages for {}".format(con))
        for m1 in winnings[con]:
            for m2 in winnings[con][m1]:
                if m1 != m2:
                    num_better, num_worse, total, mean_worse, mean_worse_perc = winnings[con][m1][m2]
                    print("{} outperforms {} in {:.3f}%".format(m1,m2, num_better/total*100))
                    print("{} was worse than {} {} times (average difference: {} ({}))".format(m1, m2, num_worse, mean_worse, mean_worse_perc))

    if file_path:
        for con in winnings:
            with open(file_path+os.path.sep+"Winnings"+con+".csv", "w") as f:
                header = "Model"
                for m in all_results:
                    header += ";{};{} Worse;{} WorseP".format(m,m,m)
                f.write(header +"\n")
                for m in winnings[con]:
                    row = "{}".format(NAME_MAP.get(m,m))
                    for metrics in winnings[con][m].values():
                        if metrics[0] != None:
                            row += ";{:.3f};{:.3f};{:.3f}".format(metrics[0]/metrics[2],metrics[3],metrics[4]) 
                        else:
                            row += ";nan;nan;nan"
                    f.write("{}\n".format(row))


def prepare_parameter_eval(base_path, parameter):
    methods_results = os.listdir(base_path)
    methods_results = [m for m in methods_results if not ".csv" in m and not "plots" in m and not ".json" in m]
    all_results = {}
    for method in methods_results:
        all_results[method] = {}
        combination_files = os.listdir(os.path.join(base_path,method))
        for f in combination_files:
            if os.path.exists(os.path.join(base_path,method,f,"results.json")):
                print("Reading file: ", os.path.join(base_path,method,f))
                exp_results = ExperimentResult.from_json(os.path.join(base_path,method,f,"results.json"))
                all_total_durations = []
                all_eval_durations = []
                all_selection_durations = []
                for con in exp_results.run_results:
                    for run_res in exp_results.run_results[con]:
                        timings_action = [metric["time_taken"] for metric in run_res["metrics"]]
                        timings_selection = [metric["selection_time"] for metric in run_res["metrics"]]
                        timings_combined = [metric["time_taken"] + metric["selection_time"] for metric in run_res["metrics"]]
                        all_total_durations.append(sum(timings_combined))
                        all_eval_durations.append(sum(timings_action))
                        all_selection_durations.append(sum(timings_selection))

                par_value = exp_results.params["strategy_parameter"][method][parameter]
                all_results[method][par_value] = {"accuracy": exp_results.overall_score, "duration_total": np.mean(all_total_durations), "duration_eval": np.mean(all_eval_durations), "duration_selection": np.mean(all_selection_durations)}
        
    with open(base_path + os.path.sep + "{}_effect.csv".format(parameter), "w") as f:
        header = "{}".format(parameter)
        for method in all_results:
            header += ";{0}Accuracy;{0}DurationTotal;{0}DurationEval;{0}DurationSelection".format(method)
        header += "\n"
        f.write(header)
        for p_val in sorted(all_results[method]):
            row = "{}".format(p_val)
            for method in all_results:
                row += ";{};{};{};{}".format(all_results[method][p_val]["accuracy"],all_results[method][p_val]["duration_total"],all_results[method][p_val]["duration_eval"],all_results[method][p_val]["duration_selection"])
            row += "\n"
            f.write(row)

def compute_stats(all_results):
    stats = {}
    m_stats = {}
    conditions = []
    all_scores_stats = {}

    for method in all_results:
        m_results = all_results[method]


        stats[method] = {}
        m_stats[method] = {}
        all_scores_stats[method] = {}
        
        all_scores = []
        all_scores_map = {}
        all_timings_map = {}
        all_fair_scores = []
        all_times = []
        for con in m_results:

            if not con in all_scores_stats[method]:
                all_scores_stats[method][con]= []

            m_stats[method][con] = {}
            conditions.append(con)
            con_final_scores = []
            con_final_fair_scores = []
            con_durations = []
            for map_id in m_results[con]:
                if not map_id in all_scores_map:
                    all_scores_map[map_id] = []
                    all_timings_map[map_id] = []
                map_scores = []
                map_timings = []
                for run in m_results[con][map_id]:
                    con_final_scores.append(run["scores"][-1])
                    con_final_fair_scores.append(run["fair_scores"][-1])
                    map_scores.append(run["scores"][-1])
                    timings = run["timings_combined"]
                    try:
                        duration = sum([sum(el) for el in timings])
                    except:
                        duration = sum(timings)
                    con_durations.append(duration)
                    map_timings.append(duration)

                all_scores_map[map_id].extend(map_scores)
                all_timings_map[map_id].extend(map_timings)
                m_stats[method][con][map_id] = {"Scores": (np.mean(map_scores), np.std(map_scores)),
                                                "Duration":(np.mean(map_timings), np.std(map_timings)) }
            all_scores.extend(con_final_scores)
            all_fair_scores.extend(con_final_fair_scores)
            all_times.extend(con_durations)

            all_scores_stats[method][con].extend(con_final_fair_scores)

            stats[method][con] = {"Scores": (np.mean(con_final_scores), np.std(con_final_scores)), 
                                "FairScores": (np.mean(con_final_fair_scores), np.std(con_final_fair_scores)),
                                "Duration": (np.mean(con_durations), np.std(con_durations))}
            print("Method: {} - Con: {}".format(method, con))
            print("Average scores: {} ({})".format(np.mean(con_final_scores), np.std(con_final_scores)))
            print("Average fair scores: {} ({})".format(*stats[method][con]["FairScores"]))
            print("Average durations: {} ({})".format(np.mean(con_durations), np.std(con_durations)))

        stats[method]["Overall"] = {"Scores": (np.mean(all_scores), np.std(all_scores)), 
                    "FairScores":  (np.mean(all_fair_scores), np.std(all_fair_scores)), 
                    "Duration": (np.mean(all_times), np.std(all_times))}

        all_scores_stats[method]["Overall"] = [el for con in all_scores_stats[method] for el in all_scores_stats[method][con]]

        m_stats[method]["Overall"] = {m_id: {"Scores": (np.mean(all_scores_map[m_id]), np.std(all_scores_map[m_id])), 
                                            "Duration": (np.mean(all_timings_map[m_id]), np.std(all_timings_map[m_id]))} for m_id in all_scores_map}

    return stats, m_stats, all_scores_stats, conditions

def compute_comparison(base_path, all_results=None):
    if all_results is None:
        all_results = read_results(base_path)

    stats, m_stats, all_scores_stats, conditions = compute_stats(all_results)
    models = list(stats.keys())
    for con in stats[models[0]]:
        with open(base_path + os.path.sep + "comparisonStats{}.csv".format(con), "w") as f:
            f.write("Model;Softmax;Score Mean;Score Std;Fair Score Mean;Fair Score Std;Duration Mean;Duration Std\n")
            for m in sorted(stats):
                m_name = m[:-len("Softmax")] if "Softmax" in m else m
                softmax = True if "Softmax" in m else False
                f.write("{};{};{};{};{};{};{};{}\n".format(NAME_MAP.get(m_name, m_name), softmax, *stats[m][con]["Scores"], 
                    *stats[m][con]["FairScores"],
                    *stats[m][con]["Duration"]))

        with open(base_path + os.path.sep + "comparisonStatistics{}.csv".format(con), "w") as f:
            header = "Model"
            for m in all_scores_stats:
                header += ";{0} Stat;{0} pval".format(m)
            f.write(header +"\n")
            for m in all_scores_stats:
                row = "{}".format(NAME_MAP.get(m,m))
                for m2 in all_scores_stats:
                    if m != m2:
                        means_m1 = all_scores_stats[m][con]
                        means_m2 = all_scores_stats[m2][con]
                        try:
                            w_stat, w_p = scipy.stats.wilcoxon(means_m1, means_m2)
                            row += ";{:.3f};{}".format(w_stat, w_p) 
                        except ValueError:
                            row += ";nan;nan"
                    else:
                        row += ";nan;nan"
                f.write("{}\n".format(row))

    with open(base_path + os.path.sep + "comparisonStatsOverall.csv", "w") as f:
        f.write("Model;Softmax;Score Mean;Score Std;Fair Score Mean;Fair Score Std;Duration Mean;Duration Std\n")
        for m in sorted(stats):
            m_name = m #[:-len("Softmax")] if "Softmax" in m else m
            softmax = True if "Softmax" in m else False
            f.write("{};{};{};{};{};{};{};{}\n".format(NAME_MAP.get(m_name, m_name), softmax, *stats[m]["Overall"]["Scores"], 
                        *stats[m]["Overall"]["FairScores"],
                        *stats[m]["Overall"]["Duration"]))


    for map_id in m_stats[models[0]]["Overall"]:
        with open(os.path.join(base_path, "comparisonAccuraciesM{}.csv".format(map_id)), "w") as f:
            f.write("Model;Overall Mean;Overall Std;NU Mean;NU Std;DU Mean;DU Std;PU Mean;PU Std\n")
            for m in sorted(m_stats):
                f.write("{};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f}\n".format(NAME_MAP.get(m, m), *m_stats[m]["Overall"][map_id]["Scores"], 
                                                                *m_stats[m]["C1"][map_id]["Scores"],
                                                                *m_stats[m]["C2"][map_id]["Scores"],
                                                                *m_stats[m]["C3"][map_id]["Scores"],))

    with open(base_path + os.path.sep + "comparisonAccuracies.csv", "w") as f:
        f.write("Model;Overall Mean;Overall Std;OverallFair Mean;OverallFair Std;NU Mean;NU Std;NUFair Mean;NUFair Std;DU Mean;DU Std;DUFair Mean;DUFair Std;PU Mean;PU Std;PUFair Mean;PUFair Std\n")
        for m in sorted(stats):
            f.write("{};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f}\n".format(NAME_MAP.get(m, m), *stats[m]["Overall"]["Scores"], 
                                                            *stats[m]["Overall"]["FairScores"], 
                                                            *stats[m]["C1"]["Scores"],
                                                            *stats[m]["C1"]["FairScores"],
                                                            *stats[m]["C2"]["Scores"],
                                                            *stats[m]["C2"]["FairScores"],
                                                            *stats[m]["C3"]["Scores"],
                                                            *stats[m]["C3"]["FairScores"]))        

    with open(base_path + os.path.sep + "comparisonTimes.csv", "w") as f:
        f.write("Model;Overall Mean;Overall Std;NU Mean;NU Std;DU Mean;DU Std;PU Mean;PU Std\n")
        for m in sorted(stats):
            f.write("{};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f};{:.4f}\n".format(NAME_MAP.get(m, m), *stats[m]["Overall"]["Duration"], 
                                                            *stats[m]["C1"]["Duration"],
                                                            *stats[m]["C2"]["Duration"],
                                                            *stats[m]["C3"]["Duration"]))     
    
    relative_timings = {}
    conditions.append("Overall")
    for con in conditions:
        relative_timings[con] = {}
        min_time = 10
        for m in sorted(stats):
            min_time = min(stats[m][con]["Duration"][0], min_time)
        
        for m in sorted(stats):
            relative_timings[con][m] = stats[m][con]["Duration"][0]/min_time

    with open(base_path + os.path.sep + "comparisonRelativeTimes.csv", "w") as f:
        f.write("Model;Overall;NU;DU;PU\n")
        for m in sorted(stats):
            f.write("{};{:.4f};{:.4f};{:.4f};{:.4f}\n".format(NAME_MAP.get(m, m), relative_timings["Overall"][m], 
                                                            relative_timings["C1"][m],
                                                            relative_timings["C2"][m],
                                                            relative_timings["C3"][m]))     

    return all_results

def determine_reeval(timings):
    # Fairly bad hack to get the number of reevaluations (not necessarily model switches)
    # from the switching approach 
    reevals = [time["selection_time"] for time in timings if time["selection_time"] > 0.0001]
    return len(reevals)

def determine_model_switches(model_list):
    last_m = model_list[0]
    reeval_list = []
    for i, m in enumerate(model_list[1:]):
        if m != last_m:
            reeval_list.append(i+1)
            last_m = m
    return reeval_list

def determine_switches(base_path, experiment_results=None):
    switches = {}
    overall = {}
    if experiment_results is None:
        experiment_results = read_experiments(base_path)
    for method in experiment_results: 
        confusions = np.zeros((3,3))
        if not ("switching" in method or "adaptive" in method):
            continue 
        switches[method] = {}
        overall[method] = {}
        m_res = experiment_results[method]
        for con in m_res.run_results:
            # switches[method][con] = []
            map_n, c_n, v_n = split_condition(con)
            if not c_n in overall[method]:
                overall[method][c_n] = []
            if not map_n in switches[method]:
                switches[method][map_n] = {}
            if not v_n in switches[method][map_n]:
                switches[method][map_n][v_n] = {}
            if not c_n in switches[method][map_n][v_n]:
                switches[method][map_n][v_n][c_n] = []
            for run in m_res.run_results[con]:
                model_list = run["models"]
                model_switches = determine_model_switches(model_list)
                if "switching" in method:
                    reevals = determine_reeval(run["metrics"])
                else:
                    reevals = len(model_list)
                switches[method][map_n][v_n][c_n].append((len(model_switches),reevals))
                overall[method][c_n].append((len(model_switches), reevals))


    method_order = list(switches.keys())
    # method_order = ["switching",first_method] #BAD HACK!
    if "switching" in method_order:
        method_order.remove("switching")
        method_order.insert(0, "switching")
    first_method = method_order[0]

    with open(base_path + os.path.sep + "modelSwitchesCompact.csv", "w") as f:
        header = "Model;OverallSwitches;OverallReeval;NUSwitches;NUReeval;DUSwitches;DUReeval;PUSwitches;PUReeval\n"
        f.write(header)
        for method in method_order:
            tmp_overall = [t for con in overall[method] for t in overall[method][con]]
            model_switches_overall = [t[0] for t in tmp_overall]
            reevals_overall = [t[1] for t in tmp_overall]
            row = "{}".format(method)
            row += ";{:.2f};{:.2f}".format(np.mean(model_switches_overall), np.mean(reevals_overall))
            for con in sorted(overall[method]):
                tmp = overall[method][con]
                model_switches = [t[0] for t in tmp]
                reevals = [t[1] for t in tmp]
                row += ";{:.2f};{:.2f}".format(np.mean(model_switches), np.mean(reevals))

            row += "\n"
            f.write(row)

    with open(base_path + os.path.sep + "modelSwitches.csv", "w") as f:
        header = "Maze"
        for method in method_order:
            header += ";OverallSwitches_{0};OverallReeval_{0};NUSwitches_{0};NUReeval_{0};DUSwitches_{0};DUReeval_{0};PUSwitches_{0};PUReeval_{0}".format(method)
        f.write(header + "\n")
        for map_n in sorted(switches[first_method]):
            for v_n in sorted(switches[first_method][map_n]):
                maze_id = "M{}V{}".format(map_n, v_n)
                row = "{}".format(maze_id)
                for method in method_order:
                    # Get Overall
                    tmp_overall = [t for con in switches[method][map_n][v_n] for t in switches[method][map_n][v_n][con]]
                    model_switches_overall = [t[0] for t in tmp_overall]
                    reevals_overall = [t[1] for t in tmp_overall]
                    row += ";{:.2f};{:.2f}".format(np.mean(model_switches_overall), np.mean(reevals_overall))
                    for con in sorted(switches[method][map_n][v_n]):
                        tmp = switches[method][map_n][v_n][con]
                        model_switches = [t[0] for t in tmp]
                        reevals = [t[1] for t in tmp]
                        row += ";{:.2f};{:.2f}".format(np.mean(model_switches), np.mean(reevals))
                row += "\n"
                f.write(row)
        # Entire overall mazes 
        row = "Overall"
        for method in method_order:
            tmp_overall = [t for con in overall[method] for t in overall[method][con]]
            model_switches_overall = [t[0] for t in tmp_overall]
            reevals_overall = [t[1] for t in tmp_overall]
            row += ";{:.2f};{:.2f}".format(np.mean(model_switches_overall), np.mean(reevals_overall))
            for con in sorted(overall[method]):
                tmp = overall[method][con]
                model_switches = [t[0] for t in tmp]
                reevals = [t[1] for t in tmp]
                row += ";{:.2f};{:.2f}".format(np.mean(model_switches), np.mean(reevals))
        row += "\n"
        f.write(row)
        
def determine_confusion_matrix(base_path):
    condition_models = {"TrueWorldModel": 1,
                        "GoalBeliefModel": 2,
                        "FreeSpaceModel": 3}
    conditions_to_names = {1: "NU",
                            2: "DU",
                            3: "PU"}
    results = read_experiments(base_path)

    for method in results: 
        confusions = np.zeros((3,3))
        if not ("switching" in method or "adaptive" in method):
            continue 
        m_res = results[method]
        for con in m_res.run_results:
            map_n, c_n, v_n = split_condition(con)
            for run in m_res.run_results[con]:
                final_model = run["models"][-1]
                final_class = condition_models[final_model]
                confusions[c_n-1,final_class-1] += 1

        print("Confusion Matrix for method {}: {}".format(method, confusions))

        with open(base_path+os.path.sep+"ConfusionsReadable{}.csv".format(method), "w") as f:
            f.write("Actual;NU;DU;PU\n")
            for i, row in enumerate(confusions):
                actual_condition = conditions_to_names[i+1]
                f.write("{};{};{};{}\n".format(actual_condition,*row))

        with open(base_path+os.path.sep+"ConfusionsPlot{}.csv".format(method), "w") as f:
            f.write("X;Y;Count\n")
            for idx, count in np.ndenumerate(confusions):
                f.write("{};{};{}\n".format(idx[1],idx[0],count))

    used_models = count_models(base_path,experiment_results=results)
    print("used models: ", used_models)
    for method in used_models: 
        with open(base_path+os.path.sep+"ConfusionsPlot{}AllModels.csv".format(method), "w") as f:
            f.write("X;Y;Count\n")
            for con, counter in used_models[method].items():
                for i, m in enumerate(["TrueWorldModel", "GoalBeliefModel", "FreeSpaceModel"]):
                    f.write("{};{};{}\n".format(i, con-1, counter.get(m,0)))   
    

def split_condition(condition):
    match_obj = re.match("condMap(\d{1,})_C(\d{1,})_V(\d{1,})", condition)
    if match_obj:
        map_n = match_obj.group(1)
        c_n = match_obj.group(2)
        v_n = match_obj.group(3) 

        return int(map_n), int(c_n), int(v_n)

    raise AttributeError("Could not match {}".format(condition))


def evaluate_parameters(id, combination, result_path, conditions, strategy_to_fit, strategy_parameters, model_parameters):
    # print("working on combination {}/{}".format(i+1,len(outcomes)))
    strategy_params = dict(strategy_parameters)
    model_params = dict(model_parameters)
    parameter_result_path = result_path + os.path.sep + "{}".format(id)
    Path(parameter_result_path).mkdir(parents=True, exist_ok=True)
    for p, value in combination:
        if p in model_params:
            model_params[p] = value
        if p in strategy_params:
            strategy_params[p] = value
        if strategy_to_fit in strategy_params and p in strategy_params[strategy_to_fit]:
            strategy_params[strategy_to_fit][p] = value
    results = rate_episodes(None, parameter_result_path, 
                                                strategy=strategy_to_fit, 
                                                model_parameters=model_params, 
                                                strategy_parameters=strategy_params,
                                                conditions=conditions)

def fit_meta_params(base_path, result_base, strategy_to_fit, strategy_parameters, model_parameters, fit_parameters):
    result_path = result_base + os.path.sep + "{}".format(strategy_to_fit)
    Path(result_path).mkdir(parents=True, exist_ok=True)
    with open(result_path + os.path.sep + "fitOptions.csv", "w") as f:
        f.write(json.dumps(fit_parameters, default=convert_numpy))
    
    conditions = playback.crawl_results(base_path + os.path.sep + "Participant_data", use_caching=USE_CACHING)

    outcome_lists = []
    for p, options in fit_parameters.items():
        if isinstance(options, tuple):
            outcome_lists.append([(p, v) for v in np.arange(*options)])
        else:
            # If this is a list of options
            outcome_lists.append([(p, o) for o in options])

    outcomes = itertools.product(*outcome_lists)
    outcomes = list(outcomes)
    print("Evaluating {} combinations".format(len(outcomes)))
    outcomes = list(zip(range(len(outcomes)), outcomes))
    eval_func = functools.partial(evaluate_parameters, result_path=result_path, conditions=conditions, strategy_to_fit=strategy_to_fit, strategy_parameters=strategy_parameters, model_parameters=model_parameters)
    with multiprocessing.Pool(6) as p:
        p.starmap(eval_func, outcomes)
    print("Finished parameter combinations")
    evaluate_best_params(result_path)


def evaluate_best_params(result_path):
    best_params = {True: None, False: None}
    best_results = {True: None, False: None}
    best_score = {True: float('inf'), False: float('inf')}
    best_run = {True:None, False: None} 

    strategy_to_fit = os.path.basename(result_path)
    combination_files = os.listdir(result_path)
    for f in combination_files:
        if os.path.exists(os.path.join(result_path,f,"results.json")):
            exp_results = ExperimentResult.from_json(os.path.join(result_path,f,"results.json"))
            softmax = exp_results.params["strategy_parameter"]["use_softmax"]
            if exp_results.overall_score < best_score[softmax]:
                best_score[softmax] = exp_results.overall_score
                best_params[softmax] = dict(exp_results.params)
                best_results[softmax] = exp_results
                best_run[softmax] = f

    for softmax in best_params:
        if best_results[softmax] is None:
            continue
        best_results[softmax].write_model_params(result_path, file_name="bestParams{}.csv".format("Softmax" if softmax else ""))
        best_results[softmax].dump(result_path, file_name="bestResults{}.json".format("Softmax" if softmax else ""))
        print("Best parameters for strategy {}{} with score {} (run {}): {}".format(strategy_to_fit, "Softmax" if softmax else "", best_score[softmax], best_run[softmax], best_params[softmax]))



def create_aamas_data(path, run_adaptive=True, run_individual=True, 
                    run_switching=True, strategy_params=None, 
                    model_params=None, 
                    num_adaptive_repititions=1,
                    softmax_variations=(True,False),
                    discount_values=(1,0.99,0.9)):

    if strategy_params is None:
        strategy_params = {}
    if model_params is None:
        model_params = {"beta":1}

    result_base = os.path.dirname(__file__) + os.path.sep + "results_thesis" + os.path.sep + "aamasThesisFinalWithDiscount"
    for use_softmax in softmax_variations:
        for discount in discount_values:
            strategy_params["use_softmax"] = use_softmax
            model_params["discount"] = discount
            if run_adaptive:
                for run_id in range(num_adaptive_repititions):
                        results_flexible = result_base + os.path.sep + "adaptive{}_D{}_{}".format("Softmax" if use_softmax else "", discount, run_id)
                        Path(results_flexible).mkdir(parents=True, exist_ok=True)
                        results = rate_episodes(path + os.path.sep + "Participant_data", results_flexible, 
                                                        strategy="adaptive", 
                                                        model_parameters=model_params, 
                                                        strategy_parameters=strategy_params)
            if run_individual:
                # Individual baselines
                for strategy in ("TrueWorld", "GoalBelief", "FreeSpace", "BToM"):
                    results_flexible = result_base + os.path.sep + "{}{}_D{}".format(strategy, "Softmax" if use_softmax else "", discount)
                    Path(results_flexible).mkdir(parents=True, exist_ok=True)
                    results = rate_episodes(path + os.path.sep + "Participant_data", results_flexible, 
                                                        strategy=strategy, 
                                                        model_parameters=model_params, 
                                                        strategy_parameters=strategy_params)
            if run_switching:
                for strategy in ["switching"]: 
                    results_flexible = result_base + os.path.sep + "{}{}_D{}_T12".format(strategy, "Softmax" if use_softmax else "",discount)
                    Path(results_flexible).mkdir(parents=True, exist_ok=True)
                    results = rate_episodes(path + os.path.sep + "Participant_data", results_flexible, 
                                                        strategy=strategy, 
                                                        model_parameters=model_params, 
                                                        strategy_parameters=strategy_params)
    return result_base


def create_goal_inference_model_data(path_goal_inferene, result_base, strategy_parameters, model_parameters):
    conditions = {}
    for f in os.listdir(path_goal_inferene):
        if not "Definition" in f:
            continue
        matchObj = re.match("Definition_Recording(\d{1,})", f)
        if matchObj:
            rec_n = matchObj.group(1)
        conditions["Recording{}".format(rec_n)] = [playback.load_experiment(path_goal_inferene + os.path.sep + f)]
    
    use_softmax = False 

    models = ["adaptive", "switching", "TrueWorld", "GoalBelief", "BToM"]
    running_params = dict(strategy_parameters)
    if strategy_parameters["adaptive"].get("keep_mentalizer", False):
        running_params["adaptive"]["regression"] = {}

    results = {}
    for strategy in models:
        model_results = ExperimentResult(strategy, strategy_parameter=strategy_parameters, model_parameter=model_parameters)
        for i, condition in enumerate(conditions):
            if not condition in results:
                results[condition] = {}
            results[condition][strategy] = []
            for env, targets, agent, true_goal in conditions[condition]:
                agent.reset(force=True)
                run_res, new_regression_params = main.rate_episode_flexible(env, targets, agent, true_goal,
                                strategy=strategy, condition=condition, strategy_parameters=running_params, model_parameters=model_parameters)
                model_results.add_run(run_res, condition)
                if strategy_parameters["adaptive"].get("keep_mentalizer", False):
                    running_params["adaptive"]["regression"] = new_regression_params
                results[condition][strategy].append(run_res)

    for m in models:
        results_flexible = result_base + os.path.sep + "{}{}".format(m, "Softmax" if use_softmax else "")
        Path(results_flexible).mkdir(parents=True, exist_ok=True)

        for condition in results:
            for i, res in enumerate(results[condition][m]):
                with open(results_flexible+os.path.sep + condition+".csv", "w") as f:
                    f.write("Step;ActionProb;AccScore;Desires;GoalBelief;SpaceBelief;Model\n")
                    for step in range(len(res["probs"])):
                        f.write("{};{};{};{};{};{};{}\n".format(step, res["probs"][step], 
                                                            res["accumulated_scores"][step],
                                                            res["inferred_state"][step]["desire"],
                                                            res["inferred_state"][step].get("goalBelief","TrueGoalBelief"),
                                                            res["inferred_state"][step].get("spaceBelief","TrueSpaceBelief"),
                                                            res["models"][step].name
                                                            ))
    

def create_model_switch_study_data(path_switch_study_conditions, result_path, strategy_parameters, model_parameters):
    conditions = {}
    for f in os.listdir(path_switch_study_conditions):
        matchObj = re.match("condition(\d{1,})-(\d{1,})", f)
        if matchObj:
            distance = matchObj.group(1)
            c_n = matchObj.group(2)
        with open(os.path.join(path_switch_study_conditions,f), "r") as _json_file:
            conditions["D{}_C{}".format(distance, c_n)] = json.load(_json_file)

    models = ["adaptive", "switching", "TrueWorld", "GoalBelief", "BToM"] 

    action_predictions = {}
    required_conditions = ["D1_C1", "D2_C1"]

    running_params = dict(strategy_parameters)
    if strategy_parameters["adaptive"].get("keep_mentalizer", False):
        running_params["adaptive"]["regression"] = {}
    
    for strategy in models:
        model_results = ExperimentResult(strategy, strategy_parameter=strategy_parameters, model_parameter=model_parameters)
        for i, condition in enumerate(required_conditions):
            condition_json = conditions[condition]
            qps = condition_json["query_points"]
            true_goal = None # Not needed anymore
            targets = {}
            for t in condition_json["exits"]:
                targets[tuple(t["pos"])] = t
            env = GridEnvironment(condition_json["map"])
            env.initialize_targets(targets)
            agent = playback.PlaybackAgent("0",["",""],tuple(condition_json["agent"]["startPos"]), env)
            agent.actions = condition_json["agent"]["behavior"]["script"]

            episode = agent.unroll()

            if condition not in action_predictions:
                action_predictions[condition] = {}
            if strategy not in action_predictions[condition]:
                action_predictions[condition][strategy] = {}

            extra_data = None
            mentalizer = Mentalizer(selection_strategy=strategy, selector_parameters=strategy_parameters, model_parameters=model_parameters)
            mentalizer.initialize_tom_models(env, targets)
            mentalizer.bias("desire","3")
            qp_n = 1
            for i,state in enumerate(episode):
                mentalizer.bias("desire","3")
                prob, used_model, predicted_score, accumulated_score, others_model, metrics = mentalizer.update(state, extra=extra_data)
                mentalizer.bias("desire","3")
                if i in qps:
                    observations = get_observations(state)
                    action_probs = used_model.get_action_predictions(observations)
                    action_probs = pos_to_dir(state,action_probs)
                    action_probs["pos"] = state
                    action_predictions[condition][strategy][qp_n] = dict(action_probs)
                    qp_n += 1

    human_data = {"D1_C1": {3:{"Up":0.65, "Down":0.35}},
                "D1_C3": {3:{"Down":0.65, "Up":0.35}},
                "D2_C1":{3:{"Up":0.45, "Down":0.55}}, 
                "D2_C3":{3:{"Down":0.45, "Up":0.55}}
                }
    for cond in action_predictions:
        print("Cond: ", cond)
        for model in action_predictions[cond]:
            print("{}: {}".format(model, action_predictions[cond][model][3]))
            model_arr = [action_predictions[cond][model][3][_dir] for _dir in ["Up","Down"]]
            model_arr = [m/sum(model_arr) for m in model_arr]
            human_arr = [human_data[cond][3][_dir] for _dir in ["Up","Down"]]
            cor, p = scipy.stats.pearsonr(human_arr, model_arr)
            print("Model achieved correlation {} ({})".format(cor,p))
        
    COND_NAMES = {"D1_C1": "L",
                    "D2_C1": "S"}
    DIRECTIONS = {1: ["Left","IDK","Right"],
                2: ["Up","IDK","Right"],
                3: ["Up","IDK","Down"]}
    
    with open(result_path+os.path.sep+"switchingStudyModelPredictions.csv", "w") as f:
        header = "Direction"
        for cond in action_predictions:
            for model in action_predictions[cond]:
                for qp in action_predictions[cond][model]:
                    header += ";{}_{}_{}".format(COND_NAMES[cond], qp, model)
        f.write(header + "\n")
        for i,_dir in enumerate(["Closest","IDK","Actual"]):
            row = "{}".format(_dir)
            for cond in action_predictions:
                for model in action_predictions[cond]:
                    for qp in action_predictions[cond][model]:
                        actual_dir = DIRECTIONS[qp][i]
                        model_arr = [action_predictions[cond][model][qp].get(t,0) for t in DIRECTIONS[qp]]
                        model_norm = sum(model_arr)
                        row += ";{}".format(action_predictions[cond][model][qp].get(actual_dir, 0)/model_norm)
            f.write(row+"\n")

def read_goal_inference_data(base_path):
    results = {}
    methods_results = os.listdir(base_path)
    methods_results = [m for m in methods_results if not ".csv" in m and not "plots" in m]
    for method in methods_results:
        if not method in results:
            results[method] = {}
        path = base_path + os.path.sep + method
        result_files = os.listdir(path)
        result_files = [rf for rf in result_files if "csv" in rf and not "RegressionParams" in rf and not "modelParams" in rf]
        for r_file in result_files:
            r_path = base_path + os.path.sep + method + os.path.sep + r_file
            run_name, _ = r_file.split(".")
            if not run_name in results[method]:
                results[method][run_name] = []
            with open(r_path, "r") as f:
                for i, line in enumerate(f):
                    if i == 0:
                        continue 
                    splits = line.strip().split(";")
                    if len(splits) == 7:
                        step, action_prob, acc_score, desires, goal_belief, space_belief, model = splits

                    desires = literal_eval(desires)
                    results[method][run_name].append({"step":step, "action_prob": action_prob, 
                                                        "desires": desires,
                                                        })

    return results

def extract_goal_inferences(base_path):
    results = read_goal_inference_data(base_path)
    QP_MAP = {"Recording1": {7:5, 16:12, 24:17, 28:21, 34:25, 41:29},
                "Recording2": {12:5, 59:28, 71:32, 96:43, 105:47, 114:51},
                "Recording3": {11:11, 18:18, 32:31, 43:41},
                "Recording4": {22:18, 32:26, 72:58, 106:81, 109:84, 132:103}}
    desires = ["R","B","O","Y","U","Yes","No"]

    for run in QP_MAP:
        with open(base_path + os.path.sep + "{}DesireInferences.csv".format(run), "w") as f:
            header = "Desire"
            for m in results:
                for i, qp in enumerate(QP_MAP[run].keys()):
                    header += ";{}QP{}".format(m, i+1)
            header += "\n"
            f.write(header)
            for d in desires:
                row = "{}".format(d)
                for m in results:
                    for qp, actual_step in QP_MAP[run].items():
                        row += ";{:.3f}".format(results[m][run][actual_step]["desires"].get(d,0))
                row += "\n"
                f.write(row)

def extract_disappearing_example(base_path, result_path, strategy_parameters, model_parameters):
    run_path = base_path + os.path.sep + "Participant_data/36/condMap4_C2_V1"
    environment, targets, agent, true_goal = playback.load_experiment(run_path)

    inferred_state = {}

    for softmax in (True, False):
        agent.reset(force=True)
        strategy_parameters["use_softmax"] = softmax
        run_res, new_regression_params = main.rate_episode_flexible(environment, targets, agent, true_goal,
                                    strategy="TrueWorld", condition="condMap4_C2_V1", strategy_parameters=strategy_parameters, model_parameters=model_parameters)
    
        inferred_state[softmax] = run_res["inferred_state"]

    print("inferred state: ", inferred_state)
    desire_order = list(inferred_state[True][0]["desire"].keys())
    col_names= {"B":"Blue", "Y": "Yellow", "R": "Red", "O":"Orange"}
    desire_order = [col_names[d] for d in desire_order]
    print("desire order: ", desire_order)
    with open(result_path + os.path.sep + "DesireTraceTWD.csv", "w") as f:
        f.write("Step;{};{};{};{};POI;{}Softmax;{}Softmax;{}Softmax;{}Softmax;POISoftmax\n".format(*desire_order,*desire_order))
        for i, state in enumerate(inferred_state[False]):
            desire = state["desire"]
            desire_softmax = inferred_state[True][i]["desire"]
            f.write("{};{};{};{};{};nan;{};{};{};{};nan\n".format(i, *desire.values(), *desire_softmax.values()))


def rate_example_traces(base_path, result_path, strategy_parameters, model_parameters)    :
    run_path_DU = base_path + os.path.sep + "Participant_data/61/condMap3_C2_V1"
    environment_DU, targets_DU, agent_DU, true_goal_DU = playback.load_experiment(run_path_DU)

    run_path_PU = base_path + os.path.sep + "Participant_data/29/condMap6_C3_V1"
    environment_PU, targets_PU, agent_PU, true_goal_PU = playback.load_experiment(run_path_PU)

    trajectories = [("condMap3_C2_V1", environment_DU, targets_DU, agent_DU, true_goal_DU), 
                    ("condMap6_C3_V1", environment_PU, targets_PU, agent_PU, true_goal_PU)]

    traces = {}
    methods = ["BToM","TrueWorld","GoalBelief","FreeSpace","switching","adaptive"]

    lengths = {}
    
    for condition, env, targets, agent, true_goal in trajectories:
        traces[condition] = {}
        lengths[condition] = len(agent.unroll())
        for method in methods:
            traces[condition][method] = {}
            for softmax in (True,False):
                agent.reset(force=True)
                strategy_parameters["use_softmax"] = softmax
                run_res, new_regression_params = main.rate_episode_flexible(env, targets, agent, true_goal,
                                            strategy=method, condition=condition, 
                                            strategy_parameters=strategy_parameters, model_parameters=model_parameters)
                fair_scores = compute_fair_scores(run_res["probs"])
                model_list = [m.name for m in run_res["models"]]

                inferred_states = run_res["inferred_state"]
                average_entropy = compute_average_entropy(inferred_states)
                reeval_list = determine_model_switches(model_list)
                traces[condition][method][softmax] = (list(fair_scores), list(run_res["accumulated_scores"]), list(reeval_list), list(average_entropy))

    for condition in traces:
        with open(result_path + os.path.sep + "ScoreList{}.csv".format(condition), "w") as f:
            header = "Step"
            for m in methods:
                header += ";{0}Fair;{0}Acc;{0}Switches;{0}Entropy;{0}FairSM;{0}AccSM;{0}SwitchesSM;{0}EntropySM".format(m)
            f.write("{}\n".format(header))
            for step in range(lengths[condition]):
                row = "{}".format(step)
                for m in methods:
                    for softmax in (False,True):
                        fair_scores, acc_scores, reevals, entropy = traces[condition][m][softmax]
                        row += ";{};{};{};{:.3f}".format(fair_scores[step], acc_scores[step], reevals[step] if step < len(reevals) else "nan", entropy[step])
                f.write("{}\n".format(row))


def entropy(distr):
    e = -sum([d*np.log(d) if d > 0 else 0 for d in distr.values()])
    return e

def compute_average_entropy(mental_state_probs):
    mental_states = ("desire","goalBelief","spaceBelief")
    domains = {"desire": 4,"goalBelief": 24,"spaceBelief":2}
    result = []
    for state in mental_state_probs:
        average_entropy = 0
        for m_s in mental_states:
            distr = state.get(m_s, {})
            average_entropy += entropy(distr)/-np.log(1/domains[m_s])
        result.append(average_entropy/3)
    return result


def transform_model_results(_results):
    QP_MAP = {"Recording1": {1:5, 2:12, 3:17, 4:21, 5:25, 6:29},
                "Recording2": {1:5, 2:28, 3:32, 4:43, 5:47, 6:51},
                "Recording3": {1:11, 2:18, 3:31, 4:41},
                "Recording4": {1:18, 2:26, 3:58, 4:81, 5:84, 6:103}}

    results = {}
    for map_con in _results:
        results[map_con] = {}
        for qp, actual_step in QP_MAP[map_con].items():
            results[map_con][qp] = _results[map_con][actual_step]["desires"]

    return results


def compute_correlation(human_res, model_res):
    answers = ["R","B","O","Y"]

    map_order = ["Recording1","Recording2","Recording3","Recording4"]

    corrs = {}
    corrs2 = {}
    ps = {}
    ps2 = {}

    human_total_arr = []
    model_total_arr = []

    for recording in map_order:
        corrs[recording] = {}
        ps[recording] = {}

        human_map_arr= []
        model_map_arr= []
        for qp in human_res[recording]:

            # human_arr = [human_res[map_con][qp].get(answer,0) for answer in answers]
            # Split U over other results most favourably for the models
            model_norm = sum([model_res[recording][qp].get(answer,0) for answer in answers])
            human_arr = [human_res[recording][qp].get(answer,0) + human_res[recording][qp].get("U",0)*(model_res[recording][qp].get(answer,0)/model_norm)  for answer in answers]
            norm = sum(human_arr)
            human_arr = [val/norm for val in human_arr]
            model_arr = [model_res[recording][qp].get(answer,0)/model_norm for answer in answers]
            cor, p = scipy.stats.pearsonr(human_arr, model_arr)
            corrs[recording][qp] = cor
            ps[recording][qp] = p

            model_map_arr.extend(model_arr)
            human_map_arr.extend(human_arr)

        avg_corrs = sum(corrs[recording].values())/len(corrs[recording])
        avg_p = sum(ps[recording].values())/len(ps[recording])

        # Round to get rid of numerical errors
        model_map_arr = np.round(model_map_arr,10)
        cor2, p2 = scipy.stats.pearsonr(human_map_arr, model_map_arr)
        corrs2[recording] = cor2
        ps2[recording] = p2

        human_total_arr.extend(human_map_arr)
        model_total_arr.extend(model_map_arr)
        
    cor_total, p_total = scipy.stats.pearsonr(human_total_arr, model_total_arr)

    return corrs, corrs2, ps, ps2, cor_total, p_total

def load_human_results(data_path):
    NUM_QPs = {"Recording1": 6,
                "Recording2": 6,
                "Recording3": 4,
                "Recording4": 6}

    results = {}
    human_results = os.listdir(data_path)
    human_results = [hr for hr in human_results if "human_results" in hr]
    
    g1_res = {}
    g21_res = {}
    g22_res = {}
    for recording in human_results:
        matchObj = re.match("human_results_Recording(\d{1,})", recording)
        if matchObj:
            rec_n = matchObj.group(1)
        rec_n = "Recording{}".format(rec_n)
        if not rec_n in g1_res:
            g1_res[rec_n] = {}
            g21_res[rec_n] = {}
            g22_res[rec_n] = {}
        path = data_path + os.path.sep + recording
        with open(path, "r") as f:
            for i, line in enumerate(f):
                if i == 0:
                    continue 
                splits = line.strip().split(";")
                i = 1
                g1_res[rec_n][splits[0]] = {qp+1: int(num) for qp,num in enumerate(splits[i:i+NUM_QPs[rec_n]])}
                i += NUM_QPs[rec_n]
                g21_res[rec_n][splits[0]] = {qp+1: int(num) for qp,num in enumerate(splits[i:i+NUM_QPs[rec_n]])}
                i += NUM_QPs[rec_n]
                g22_res[rec_n][splits[0]] = {qp+1: int(num) for qp,num in enumerate(splits[i:i+NUM_QPs[rec_n]])}

            g1_res[rec_n] = swap_nested_dict(g1_res[rec_n])
            g21_res[rec_n] = swap_nested_dict(g21_res[rec_n])
            g22_res[rec_n] = swap_nested_dict(g22_res[rec_n])

    results = {"G1": g1_res, "G2C1": g21_res, "G2C2": g22_res}

    return results

def swap_nested_dict(d):
    res = {}
    for k, nested_d in d.items():
        for n_k, n_v in nested_d.items():
            if not n_k in res:
                res[n_k] = {}
            
            res[n_k][k] = n_v

    return res

def compute_goalInference_correlations(data_path, result_path):

    human_results = load_human_results(data_path)

    human_group1 = human_results["G1"]
    human_group2C1 = human_results["G2C1"]
    human_group2C2 = human_results["G2C2"]

    all_human_results = {"Group1": human_group1, "Group2C1": human_group2C1, "Group2C2": human_group2C2}
    all_model_results = read_goal_inference_data(result_path)


    corrs = {}
    corrs2 = {}
    best_corrs2 = {}
    ps = {}
    ps2 = {}
    total_cors = {}
    best_total_corr = {}
    total_ps = {}

    for model, m_results in all_model_results.items():
        m_result = transform_model_results(m_results)
        if not model in corrs:
            corrs[model] = {}
            corrs2[model] = {}
            ps[model] = {}
            ps2[model] = {}
            total_cors[model] = {}
            total_ps[model] = {}
        for h_group, human_res in all_human_results.items():
            corrs[model][h_group], corrs2[model][h_group], ps[model][h_group], ps2[model][h_group], total_cors[model][h_group], total_ps[model][h_group] = compute_correlation(human_res, m_result)

    with open(result_path + os.path.sep + "Correlations.csv", "w") as f:
        header = "Model"
        for human_group in all_human_results:
            for i, condition in enumerate(all_human_results[human_group]):
                header += ";Recording{}{}".format(i+1, human_group)
            header += ";Overall{}".format(human_group)
        header += "\n"
        f.write(header)
        for model in corrs2:
            row = "{}".format(model)
            for human_group in all_human_results:
                for i, condition in enumerate(all_human_results[human_group]):
                    row += ";{:.2}".format(corrs2[model][human_group][condition])
                row += ";{:.2}".format(total_cors[model][human_group])
            row += "\n"
            f.write(row)

def load_regression_params(path):
    if ".pickle" in path:
        with open(path, "rb") as f:
            exp_run = pickle.load(f)
    elif ".json" in path:
        exp_run = ExperimentResult.from_json(path)

    return dict(exp_run.last_regression_params) 


def pos_to_dir(state, action_probs):
    y0,x0 = state
    res = {}
    for pos in action_probs:
        y1,x1 = pos 
        if (x1-x0,y1-y0) == (-1,0):
            res["Left"] = action_probs[pos]
        if (x1-x0,y1-y0) == (1,0):
            res["Right"] = action_probs[pos]
        if (x1-x0,y1-y0) == (0,-1):
            res["Up"] = action_probs[pos]
        if (x1-x0,y1-y0) == (0,1):
            res["Down"] = action_probs[pos]
    return res
        
class User(object):
    
    def __init__(self, name, ratings):
        self.name = name
        self.ratings = dict(ratings)

class Rating(object):
    
    def __init__(self):
        self.condition = ""
        self.number = ""
        self.user = ""
        self.query_points = {}
        self.start_time = None
        self.comment = ""
        
    def compute_egocentric_hits(self):
        EGOCENTRIC_ACTIONS = {
            "C1": {2:"Left", 10: "Left",13: "Left",17: "Left",22: "Up",24: "Up"},
            "C2": {2:"Left", 5: "Left", 16: "Left",23: "Up",40: "Right",52: "Up",54: "Up"},
            "C3": {3:"Left",12: "Left",15: "Left",20: "Left",27: "Left",29: "Down",52: "Left",62: "Up",64: "Up"}
        }
        scores = {}
        for q, p in self.query_points.items():
            score = 0
            likely_responses = [dir for dir in p[3] if p[3][dir] == "Likely"]
            if EGOCENTRIC_ACTIONS[self.condition][q] in likely_responses:
                score = 1
            scores[q] = (score, len(likely_responses))

        return scores

    def compute_accuracy(self, path_to_conditions):
        # with open(path_to_conditions+os.path.sep+"condMap2_{}_V2".format(self.condition), "r") as f:
        #     pb_string = f.read()
        environment, targets, agent, true_goal = playback.load_experiment(path_to_conditions+os.path.sep+"condMap2_{}_V2".format(self.condition))
        scores = {}
        for q, p in self.query_points.items():
            if self.condition == "C2" and q == 40:
                performed_action = agent.actions[q+1]
            else:
                performed_action = agent.actions[q]
            score = 0
            count = 0
            for p_dir in p[3]:
                if p_dir in performed_action[1] and p[3][p_dir] == "Likely":
                    score = 1
                    count = list(p[3].values()).count("Likely")
            scores[q] = (score, count)
        return scores
                
    @classmethod
    def from_file(cls, path):
        res = cls()
        
        condition_name = os.path.basename(path)
        match_obj = re.match("recMap2_(C\d)_V2", condition_name)
        if match_obj:
            res.condition = match_obj.group(1)
        
        with open(path, "r", encoding="utf8") as f:
            for i, l in enumerate(f.readlines()):
                if i == 0:
                    stime = l[:l.find(": ")]
                    res.startTime = parse(stime)
                    res.user = l[l.find(": ")+2:]
                elif i == 1:
                    res.number = int(l[l.find("#")+1:l.find("(")].strip())
                elif i > 1 and "Comment" not in l:
                    match_obj = re.match("(.*): Step: (\d{1,}), Pos: (\(\d{1,}, \d{1,}\)), Ratings: (.*)", l)
                    if match_obj:
                        q_point = (parse(match_obj.group(1)), #Time
                                  int(match_obj.group(2)), #Step
                                  ast.literal_eval(match_obj.group(3)), #Pos
                                  ast.literal_eval(match_obj.group(4)) #Ratings
                                  )
                        res.query_points[q_point[1]] = q_point
                    else:
                        raise AttributeError("No match found for query points")
                else:
                    ctime = parse(l[:l.find(": ")])
                    comment = l[l.rfind(": ")+2:]
                    res.comment = (ctime, comment)
        return res

def collect_results_per_user(path):
    entries = os.listdir(path)
    users = [e for e in entries if os.path.isdir(path + os.path.sep +e)]
    res = {}
    for u in users:
        skip_user = False
        files = os.listdir(path + os.path.sep +u)
        ratings = {}
        for f in files:
            if "Map" in f:
                r = Rating.from_file(path + os.path.sep + u + os.path.sep + f)
                ratings[f] = r
        if not skip_user:
            res[u] = User(u, ratings)
    return res


def compute_rating_accuracy(path_switch_study, results_base):
    
    users = collect_results_per_user(path_switch_study+os.path.sep+"Participant_data")
    for u in users.values():
        accuracies = {}
        egocentric_hits = {}
        for cond in u.ratings:
            accuracies[cond] = u.ratings[cond].compute_accuracy(path_switch_study+os.path.sep+"Conditions")
            egocentric_hits[cond] =  u.ratings[cond].compute_egocentric_hits()
        u.accuracies = accuracies
        u.egocentric_bias = egocentric_hits

    aggrs = {}
    aggrs_norm = {}
    aggrs_ego = {}
    aggrs_ego_norm = {}

    NAME_SWITCH = {"condMap2_C1_V2":"NU",
                    "recMap2_C1_V2":"NU",
                    "condMap2_C2_V2":"DU",
                    "recMap2_C2_V2":"DU",
                    "condMap2_C3_V2":"PU",
                    "recMap2_C3_V2":"PU",
                    }

    for cond in list(users.values())[0].ratings:
        tmp = []
        tmp_egs = []
        tmp_norm= []
        tmp_egs_norm = []
        length= None
        
        for u in users.values():
            acs = u.accuracies[cond]
            egs = u.egocentric_bias[cond]

            accuracies = list(acs.values())
            egocentric_scores = list(egs.values())
            if length is None:
                length = len(accuracies)
            if len(accuracies) != length:
                raise Exception("missing acs in {} (should be {}). user: {}".format(acs,length, u.name))
            tmp.append([l[0] for l in accuracies])
            tmp_egs.append([l[0] for l in egocentric_scores])
            tmp_norm.append([l[0]/l[1] if l[1] > 0 else 0 for l in accuracies])
            tmp_egs_norm.append([l[0]/l[1] if l[1] > 0 else 0 for l in egocentric_scores])
        
        aggrs[cond] = np.array(tmp)
        aggrs_norm[cond] = np.array(tmp_norm)
        aggrs_ego[cond] = np.array(tmp_egs)
        aggrs_ego_norm[cond] = np.array(tmp_egs_norm)
    for con in aggrs:
        hits_mean = np.mean(aggrs[con], axis=0)
        hits_std = np.std(aggrs[con], axis=0)
        hits_norm_mean = np.mean(aggrs_norm[con], axis=0)
        hits_norm_std = np.std(aggrs_norm[con], axis=0)
        ego_hits_mean = np.mean(aggrs_ego[con], axis=0)
        ego_hits_std = np.std(aggrs_ego[con], axis=0)
        ego_hits_norm_mean = np.mean(aggrs_ego_norm[con], axis=0)
        ego_hits_norm_std = np.std(aggrs_ego_norm[con], axis=0)
        with open(results_base+os.path.sep+"humanAccuraciesAggregated{}.csv".format(NAME_SWITCH[con]), "w") as f:
            f.write("QP;PercHits;PercHitsStd;PercHitsNorm;PercHitsNormStd;EgoHits;EgoHitsStd;EgoHitsNorm;EgoHitsNormStd\n")
            for qp in range(aggrs[con].shape[1]):
                f.write("{};{};{};{};{};{};{};{};{}\n".format(qp+1, hits_mean[qp], hits_std[qp], hits_norm_mean[qp], hits_norm_std[qp],
                                                                ego_hits_mean[qp], ego_hits_std[qp], ego_hits_norm_mean[qp], ego_hits_norm_std[qp]))


def create_action_prediction_data(path_switch_study, path_aamas_recordings, 
                                results_base, strategy_parameters, model_parameters):
    users = collect_results_per_user(path_switch_study+os.path.sep+"Participant_data")
    ratings = {}
    for u in users.values():
        for rec, rating in u.ratings.items():
            if not rec in ratings:
                ratings[rec] = {}
            qp_n = 1
            for qp, vals in rating.query_points.items():
                if not qp_n in ratings[rec]:
                    ratings[rec][qp_n] = {}
                for dir, resp in vals[3].items():
                    if not dir in ratings[rec][qp_n]:
                        ratings[rec][qp_n][dir] = 0
                    if resp == "Likely":
                        ratings[rec][qp_n][dir] += 1
                qp_n += 1

    paths = {"condMap2_C1_V2": os.path.join(path_aamas_recordings, "55", "condMap2_C1_V2"),
             "condMap2_C2_V2": os.path.join(path_aamas_recordings, "116", "condMap2_C2_V2"),
             "condMap2_C3_V2":  os.path.join(path_aamas_recordings, "17", "condMap2_C3_V2")}

    methods = ["BToM","TrueWorld","GoalBelief","FreeSpace","switching","adaptive"]
    
    QPs = {"condMap2_C1_V2": [2,10,13,17,22,24],
            "condMap2_C2_V2": [2,5,16,23,39,50,52],
            "condMap2_C3_V2": [2, 10, 13, 17, 23,25,43,50,52]}


    NAME_MAP = {"condMap2_C1_V2": "recMap2_C1_V2",
            "condMap2_C2_V2": "recMap2_C2_V2",
            "condMap2_C3_V2": "recMap2_C3_V2"}

    action_predictions = {}

    for con, path in paths.items():
        env, targets, agent, true_goal = playback.load_experiment(path) 
        agent.reset(force=True)
        episode = agent.unroll()

        if con not in action_predictions:
            action_predictions[con] = {}

        extra_data = None
        # if use_features and condition != None:
        if strategy_parameters.get("use_features",False) and con != None:
            match_obj = re.match("condMap(\d{1,})_C(\d{1,})_V(\d{1,})", con)
            if match_obj:
                map_n = match_obj.group(1)
                c_n = match_obj.group(2)
                v_n = match_obj.group(3) 
                extra_data = {"map_id": int(map_n), "condition": int(c_n), "variant": int(v_n)}
        
        for method in methods:
            if not method in action_predictions[con]:
                action_predictions[con][method] = {}
            mentalizer = Mentalizer(selection_strategy=method, selector_parameters=strategy_parameters, model_parameters=model_parameters)
            mentalizer.initialize_tom_models(env, targets)#, use_softmax)
            qp_n = 1
            for i,state in enumerate(episode):
                # print("currently at state: ", state)
                prob, used_model, predicted_score, accumulated_score, others_model, metrics = mentalizer.update(state, extra=extra_data)
                if i in QPs[con]:
                    observations = get_observations(state)
                    action_probs = used_model.get_action_predictions(observations)
                    action_probs = pos_to_dir(state,action_probs)
                    action_predictions[con][method][qp_n] = dict(action_probs)
                    qp_n += 1

    directions = ["Left","Right","Up","Down"]
    with open(results_base + os.path.sep + "actionPredictions.csv", "w") as f:
        header= "QPAction;HumanC1;HumanC2;HumanC3"
        for m in methods:
            header += ";{0}C1;{0}C2;{0}C3".format(m)
        f.write(header + "\n")
        for qp in range(9): # C3 has the most qps 
            for dir in directions:
                row = "{}:{}".format(qp+1,dir)
                for con in sorted(ratings):
                    if qp+1 in ratings[con]:
                        row += ";{}".format(ratings[con][qp+1].get(dir,0)/sum(ratings[con][qp+1].values()))
                    else:
                        row += ";nan"
                for m in methods:
                    for con in sorted(action_predictions):
                        if qp+1 in action_predictions[con][m]:
                            row += ";{}".format(action_predictions[con][m][qp+1][dir])
                        else:
                            row += ";nan"
                f.write(row + "\n")

    entropies = {}
    pearsons = {}

    overall_arr_human = {}
    overall_arr_model = {}

    correlations = {}
    for con in action_predictions:
        
        if not con in entropies:
            entropies[con] = {}
            pearsons[con] = {}
            correlations[con] = {}
        print("Condition: ", con)
        human_name = NAME_MAP[con]
        for method in methods:
            if not method in overall_arr_model:
                overall_arr_model[method] = []
                overall_arr_human[method] = []
            complete_arr_human = []
            complete_arr_model = []
            for qp in ratings[human_name]:
                if not qp in entropies[con]:
                    entropies[con][qp] = {}
                    pearsons[con][qp] = {}
                print("QP: ", qp)
                arr_model = [v for k,v in action_predictions[con][method][qp].items()]
                complete_arr_model.extend(arr_model)
                human_norm = sum(ratings[human_name][qp].values())
                arr_human = [ratings[human_name][qp].get(k,0)/human_norm for k in action_predictions[con][method][qp]]
                complete_arr_human.extend(arr_human)
                entropy =  scipy.stats.entropy(arr_human, arr_model)
                pearsonr = scipy.stats.pearsonr(arr_human, arr_model)
                print("Entropy with {}: {}".format(method,entropy))
                print("Pearson with {}: {}".format(method,pearsonr))
                entropies[con][qp][method] = entropy
                pearsons[con][qp][method] = pearsonr[0]
            print("Pearson for entire condition: ", con)
            pearsonr = scipy.stats.pearsonr(complete_arr_human, complete_arr_model)
            overall_arr_human[method].extend(complete_arr_human)
            overall_arr_model[method].extend(complete_arr_model)
            print("Pearson with {}: {}".format(method,pearsonr))
            correlations[con][method] = pearsonr
    
    with open(results_base + os.path.sep + "actionPredictionCorrelations.csv", "w") as f:
        header = "Model;C1;C2;C3;Overall"
        f.write(header + "\n")
        for method in methods:
            row = "{}".format(method)
            for con in correlations:
                row += ";{}".format(correlations[con][method][0])
            pearsonr = scipy.stats.pearsonr(overall_arr_human[method], overall_arr_model[method])
            row += ";{}".format(pearsonr[0])
            f.write(row +"\n")



def count_models(base_path, experiment_results=None):
    from collections import Counter
    used_models = {}
    switches = {}
    used_models_overall = {}
    if experiment_results is None:
        experiment_results = read_experiments(base_path)
    for method in experiment_results: 
        print("checking run: ", method)
        # if not ("switching" in method or "adaptive" in method):
        #     continue 
        used_models[method] = {}
        switches[method] = {}
        used_models_overall[method] = {}
        m_res = experiment_results[method]
        for con in m_res.run_results:
            # switches[method][con] = []
            map_n, c_n, v_n = split_condition(con)
            if not c_n in used_models_overall[method]:
                used_models_overall[method][c_n] = Counter()
                switches[method][c_n] = []

            if not map_n in used_models[method]:
                used_models[method][map_n] = {}
                
            if not v_n in used_models[method][map_n]:
                used_models[method][map_n][v_n] = {}

            if not c_n in used_models[method][map_n][v_n]:
                used_models[method][map_n][v_n][c_n] = Counter()

            for run in m_res.run_results[con]:
                model_list = run["models"]
                counts = Counter(model_list)
                model_switches = determine_model_switches(model_list)
                used_models[method][map_n][v_n][c_n] += counts
                switches[method][c_n].append(len(model_switches))
                used_models_overall[method][c_n] += counts

    for m in switches:
        for c in switches[m]:
            switches[m][c] = np.mean(switches[m][c])

    print("overall results: ", used_models_overall)
    print("model switches: ", switches)
    return used_models_overall


if __name__ == "__main__":
    SEED = 0
    random.seed(SEED)
    np.random.seed(SEED)

    path_aamas = os.path.join("data", "aamas")
    playback.CONDITION_PATH = path_aamas + os.path.sep + "Conditions"

    """
        You can load previously saved regression parameters and use those instead of "default/empty" ones.
    """
    # regression_params = load_regression_params(regression_path)

    """
        Default parameters for the selection (meta-reasoning) strategies and the underlying models.
    """
    strategy_parameters = {'use_softmax': False, 
                        'full_initialization': False, 
                        'adaptive': {'keep_mentalizer': False, 
                                    'use_features': False, 
                                    'opportunity_costs': 2.5, 
                                    'time_prior': {'TrueWorldModel': 0, 'GoalBeliefModel': 0, 'FreeSpaceModel': 0}, 
                                    'score_f': 'NLL', # prob/NLL/S5
                                    'num_features': 3,
                                    'load_regression': None,
                                    # Bayesian Regression Init
                                    'alpha_prior_reward': 10, 
                                    'beta_prior_reward': 1,
                                    'alpha_prior_time': 10,
                                    'beta_prior_time': 1,
                                    "reward_weight": 10, # Scaling factor for reward scores
                                    "time_weight": 1000, # Scaling factor for time scores
                                    },
                        "switching": { "initial_threshold": 20, #1.2 for S5, #20 for NLL 
                                        "threshold_growth": 1.5,
                                        "score_f": "NLL"
                                    }
                        }
    model_parameters = {'BiasedGenerativeModel': False, 'beta':1, 'discount':1, 'alpha': 2.5, 'gamma': 0.125}


    """
        Counts the number of times participants visited a specific tile which serves as the basis 
        to generate position heatmaps.
    """
    output_path = os.path.dirname(__file__) + os.path.sep + "results"
    Path(output_path).mkdir(parents=True, exist_ok=True)
    # create_heatmap_data(path_aamas, output_path, map_name="condMap6", variant="V1")

    """
        Evaluate differnet models on the entire AAMAS data and record the result data
    """
    # result_base = create_aamas_data(path_aamas, run_adaptive=True, run_individual=False, 
    #                             run_switching=True, 
    #                             strategy_params=strategy_parameters, 
    #                             model_params=model_parameters,
    #                             softmax_variations=[True,False], 
    #                             discount_values=[1,0.99,0.9]) 
    
    """
        The following functions will always load the generated results to 
        compute the evaluation statistics, thus you only need to create the data once and can specify the 
        result_base directly to load the results directly.
    """
    result_base = os.path.dirname(__file__) + os.path.sep + "results" + os.path.sep + "aamasThesisFinalWithDiscount"

    """
        Compute model comparisons between the evaluated models based on accuracy and computational time. 
        Also prepares statistics
    """
    all_results = compute_comparison(result_base)
    """
        Compares the models head to head for each trajectory for the "winning" comparisons.
    """
    compute_winning_percentages(result_base, result_base, all_results=all_results)

    """
        Compare the meta-reasoning approaches against each other
    """
    experiment_results = determine_confusion_matrix(result_base)
    determine_switches(result_base, experiment_results=experiment_results)
    count_models(result_base, experiment_results=experiment_results)
    """
        Evaluation of example traces and the disappearing probability problem
    """
    extract_disappearing_example(path_aamas, result_base, strategy_parameters, model_parameters)
    rate_example_traces(path_aamas, result_base, strategy_parameters, model_parameters)

    """
        Evaluate models on the switching domain for evaluating factors influencing biases.
    """
    path_switch_study_conditions = os.path.join("data","switchingStudy")
    result_base = os.path.dirname(__file__) + os.path.sep + "results" + os.path.sep + "switchStudyPredictions" 
    Path(result_base).mkdir(parents=True, exist_ok=True)
    create_model_switch_study_data(path_switch_study_conditions, result_base, strategy_parameters=strategy_parameters, model_parameters=model_parameters)

    """
        Evaluation of the goal inference biases and the model's correlation to human data
    """
    path_goal_inference = os.path.join("data", "goalInference")
    result_base_goal_inference = os.path.dirname(__file__) + os.path.sep + "results" + os.path.sep + "goalInference"
    Path(result_base_goal_inference).mkdir(parents=True, exist_ok=True)
    create_goal_inference_model_data(path_goal_inference, result_base_goal_inference, strategy_parameters=strategy_parameters, model_parameters=model_parameters)

    compute_goalInference_correlations(path_goal_inference, result_base_goal_inference)

    """
        Generate the result files from the empirical studies regarding action predictions.
    """
    path_switch_study = os.path.join("data", "actionPredictions")
    results_action = os.path.dirname(__file__) + os.path.sep + "results" + os.path.sep + "actionPrediction"
    Path(results_action).mkdir(parents=True, exist_ok=True)
    

    compute_rating_accuracy(path_switch_study, results_action)
    """
        Compare empirical action predictions against models' predictions.
    """
    path_aamas_recording = os.path.join(path_aamas, "Participant_data")
    create_action_prediction_data(path_switch_study, path_aamas_recording, 
                        results_action, strategy_parameters, model_parameters)



    """
        You can also evaluate the effect of varying different parameters 
    """
    evaluate_parameter_combinations = False
    if evaluate_parameter_combinations:
        result_base = os.path.dirname(__file__) + os.path.sep + "results" + os.path.sep + "paramTestSwitchingThreshold"
        """
            fit_parameters determine which parameters should be varied for testing as well as what values 
            these parameters should take. Tuple define a range from start to end with the increment, 
            lists on the other hand determine the exact values that should be used.
        """
        fit_parameters = {
                            # "beta":(1,1.3,0.25), 
                            # "time_prior": [{"TrueWorldModel": 0,
                            #                 "GoalBeliefModel": 0,
                            #                 "FreeSpaceModel": 0
                            #                 }, {"TrueWorldModel": 0.001,
                            #                 "GoalBeliefModel": 0.001,
                            #                 "FreeSpaceModel": 0.001
                            #                 },{"TrueWorldModel": 0.001,
                            #                 "GoalBeliefModel": 0.002,
                            #                 "FreeSpaceModel": 0.002
                            #                 },
                            #                 {"TrueWorldModel": 0.001,
                            #                 "GoalBeliefModel": 0.002,
                            #                 "FreeSpaceModel": 0.006
                            #                 }
                            #                 ],
                            # "score_f": ["NLL","S5"], #["prob", "NLL"],
                            "opportunity_costs" : (5,10.1,1),
                            # "full_initialization": [True, False],
                            # "use_features" :[True,False],
                            # "keep_mentalizer": [True,False]
                            # "use_softmax": [True, False],
                            # "discount": [0.99,1]
                            # "initial_threshold": [0,1.2,20]#(1,31,)
                        }

        method = "adaptive"
        folder_name = "paramTest{}{}".format(method,list(fit_parameters.keys())[0])
        result_base = os.path.join(os.path.dirname(__file__), "results", folder_name)
        if os.path.exists(result_base):
            result_base = os.path.join(result_base,"_1")
        results_flexible = result_base + os.path.sep + "{}".format(method)
        Path(results_flexible).mkdir(parents=True, exist_ok=True)
        """
            Collect the results given teh different parameter values
        """
        fit_meta_params(path_aamas, results_flexible, method, strategy_parameters=strategy_parameters, 
                        model_parameters=model_parameters, fit_parameters=fit_parameters)

        """
            Evaluate the collected results with respect to the given parameter.
        """
        prepare_parameter_eval(result_base + os.path.sep + method , "opportunity_costs")
