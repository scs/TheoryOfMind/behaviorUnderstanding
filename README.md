# Behavior Understanding

Maintained by Jan Pöppel (jpoeppel@techfak.uni-bielefeld.de)

**This branch serves to replicate the results from the dissertation, for the general framework, 
check the `adaptiveFramework` branch instead!**

This repository contains the models and evaluation scripts used for my dissertation. 
The work primarily revolves around adaptive meta-reasoning processing for 
ToM. The dissertation presents two approaches: "switching" and "adaptive".
While the adaptive processes work differently, both make use of the same 
specialized Bayesian Theory of Mind (BToM) models.

This repository tries to implement the meta-reasoning approaches in a way 
that abstracts the specialities of the underlying specialized models away as much 
as possible.
The specialized models themselves are implemented in a general way, where they share the 
same generative model applicable for the 2D gridworld environment my work primarily considers 
and only defines different action likelihoods that take the different mental states 
into account. 

The models are primarily evaluated on the data recorded for the AAMAS 2018 
paper "Satisficing Models of Bayesian Theory of Mind for Explaining
Behavior of Differently Uncertain Agents" which can be found at 
https://doi.org/10.4119/unibi/2934879 (see also https://gitlab.ub.uni-bielefeld.de/scs/TheoryOfMind/aamas2018 
for more information on the earlier model implementation). The data required to run the 
model comparison has also been added here.

Additionally, the thesis compares the models' predictions and inferences with those performed 
by humans when seeing examples of the earlier AAMAS data.
The original data for the action predictions has not previously been published by itself but has been added here,
alongside data from "Egocentric Tendencies in Theory of Mind Reasoning: An Empirical and Computational Analysis"
(https://pub.uni-bielefeld.de/record/2935637).

These scripts can be used to evaluate the models on the data 
from "Less Egocentric Biases in Unsolicited Theory of Mind When Observing Agents in Unbalanced Decision Problems"
(urn:nbn:de:0070-pub-29552876) although this comparison was not included in the thesis for succinctness. 


## Hardware Dependencies

None 

## Supported operating systems

Linux, Windows, Mac

## Build dependencies

Python 3

## Run dependencies

* Python >= 3 (Python 2 does also work, but no conda packages will be created for Python 2)
* numpy 
* scipy 

## Installation

This evaluation repository should not be installed, as it is supposed to be configured for 
the different evaluations performed for my dissertation. 
However, you can install the adaptiveSwitching framework itself disconnected from the 
concrete experiments for the dissertation. 
The branch `adaptiveFramework` has more information and the adapted code, which can also 
be used to interface with the broader scs architecture by defining an ipaaca interface 
for getting live observations and publishing the corresponding inferences.

## Usage

All evaluations are configured and run by the script `diss_experiments.py`.
The script will create result files (generally .csv files) according to its configuration 
which were then used to create the plots in LaTeX using the packages pgfplots and pgfplotstable.

The script does, however, expect to find the relevant data it should evaluate the model on. 
The data it can work with and whose paths need to be configured in the script (the default 
assumption is that the data can be found in a `data` folder on the same level as the script)
are:

- Data from the AAMAS study (`aamas`)
    - Condition data 
    - Participant_data 
- Data from the action prediction study (`actionPredictions`)
    - Condition Data
    - Participant_data
- Data from the goal inference studies (`goalInference`)
    - Condition data as Definitions
    - Human prediction results for the different recordings
- Condition data of the switching study for evaluating factors influencing egocentric biases (`switchingStudy`)





